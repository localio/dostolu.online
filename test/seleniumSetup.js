import chromedriver from 'chromedriver';

export function testStart(webdriver) {
  chromedriver.start();
  const driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();
  return driver;
}

export function testStop(driver, done) {
  driver.quit().then(() => {
    chromedriver.stop();
    done();
  });
}
