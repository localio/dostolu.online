import CopyWebpackPlugin from 'copy-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import WebpackIsomorphicToolsPlugin from 'webpack-isomorphic-tools/plugin';
import autoprefixer from 'autoprefixer';
import config from '../src/server/config';
import constants from './constants';
import ip from 'ip';
import path from 'path';
import webpack from 'webpack';
import webpackIsomorphicAssets from './assets';
import { BundleAnalyzerPlugin } from 'webpack-bundle-analyzer';

const webpackIsomorphicToolsPlugin = new WebpackIsomorphicToolsPlugin(webpackIsomorphicAssets);

// cheap-module-eval-source-map, because we want original source, but we don't
// care about columns, which makes this devtool faster than eval-source-map.
// http://webpack.github.io/docs/configuration.html#devtool
// const devtools = 'cheap-module-eval-source-map';
// TODO: https://github.com/webpack/webpack/issues/2145
const devtools = 'eval-source-map';

const loaders = {
  css: '',
  // Why not LESS or Stylus? The battle is over, let's focus on inline styles.
  scss: '!sass-loader',
  sass: '!sass-loader?indentedSyntax'
};

const serverIp = config.remoteHotReload
  ? ip.address() // Dynamic IP address enables hot reload on remote devices.
  : 'localhost';

export default function makeConfig(options) {
  const {
    isDevelopment
  } = options;

  const stylesLoaders = Object.keys(loaders).map(ext => {
    const prefix = 'css-loader!postcss-loader';
    const extLoaders = prefix + loaders[ext];
    const loader = isDevelopment
      ? `style-loader!${extLoaders}`
      : ExtractTextPlugin.extract({
        fallbackLoader: 'style-loader',
        loader: extLoaders,
      });
    return {
      loader,
      test: new RegExp(`\\.(${ext})$`)
    };
  });

  const config = {
    // hotPort: constants.HOT_RELOAD_PORT,
    cache: isDevelopment,
    // debug: isDevelopment,
    devtool: isDevelopment ? devtools : '',
    entry: {
      app: isDevelopment ? [
        `webpack-hot-middleware/client?path=http://${serverIp}:${constants.HOT_RELOAD_PORT}/__webpack_hmr`,
        path.join(constants.SRC_DIR, 'browser/webapp/index.js')
      ] : [
        path.join(constants.SRC_DIR, 'browser/webapp/index.js')
      ]
    },
    module: {
      noParse: [
        // https://github.com/localForage/localForage/issues/617
        new RegExp('localforage.js'),
      ],
      rules: [
        {
          loader: 'url-loader',
          test: /\.(gif|jpg|png|svg)(\?.*)?$/,
          options: {
            limit: 10000,
          },
        }, {
          loader: 'url-loader',
          test: /favicon\.ico$/,
          options: {
            limit: 1,
          },
        }, {
          loader: 'url-loader',
          test: /\.(ttf|eot|woff|woff2)(\?.*)?$/,
          options: {
            limit: 100000,
          },
        }, {
          loader: 'babel-loader',
          test: /\.js$/,
          exclude: constants.NODE_MODULES_DIR,
          options: {
            cacheDirectory: true,
            presets: [['es2015', { modules: false }], 'react', 'stage-1'],
            plugins: [
              ['transform-runtime', {
                helpers: false,
                polyfill: false,
                regenerator: false,
              }],
            ],
            env: {
              production: {
                plugins: [
                  'transform-react-constant-elements',
                ],
              },
            },
          },
        },
        ...stylesLoaders,
      ],
    },
    output: isDevelopment ? {
      path: constants.BUILD_DIR,
      filename: '[name].js',
      chunkFilename: '[name]-[chunkhash].js',
      publicPath: `http://${serverIp}:${constants.HOT_RELOAD_PORT}/build/`
    } : {
      path: constants.BUILD_DIR,
      filename: '[name]-[hash].js',
      chunkFilename: '[name]-[chunkhash].js',
      publicPath: '/assets/'
    },
    plugins: (() => {
      const plugins = [
        new webpack.LoaderOptionsPlugin({
          minimize: !isDevelopment,
          debug: isDevelopment,
          // Webpack 2 no longer allows custom properties in configuration.
          // Loaders should be updated to allow passing options via loader options in module.rules.
          // Alternatively, LoaderOptionsPlugin can be used to pass options to loaders
          hotPort: constants.HOT_RELOAD_PORT,
          // postcss: () => [autoprefixer({ browsers: 'last 50 version' })],
          options: {
            postcss: () => [autoprefixer]
          }
        }),
        new webpack.DefinePlugin({
          'process.env': {
            IS_BROWSER: true, // Because webpack is used only for browser code.
            IS_SERVERLESS: JSON.stringify(process.env.IS_SERVERLESS || false),
            NODE_ENV: JSON.stringify(isDevelopment ? 'development' : 'production'),
            SERVER_URL: JSON.stringify(process.env.SERVER_URL || '')
          }
        })
      ];
      if (isDevelopment) {
        plugins.push(
          // new webpack.optimize.OccurrenceOrderPlugin(),
          new webpack.HotModuleReplacementPlugin(),
          new webpack.NoEmitOnErrorsPlugin(),
          webpackIsomorphicToolsPlugin.development(),
          new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            openAnalyzer: false
          }),
          new webpack.SourceMapDevToolPlugin({
            filename: '[file].map'
          })
        );
      } else {
        plugins.push(
          // Render styles into separate cacheable file to prevent FOUC and
          // optimize for critical rendering path.
          new ExtractTextPlugin({
            filename: 'app-[hash].css',
            disable: false,
            allChunks: true
          }),
          new webpack.optimize.OccurrenceOrderPlugin(),
          new webpack.optimize.UglifyJsPlugin({
            compress: {
              screw_ie8: true, // eslint-disable-line camelcase
              warnings: false // Because uglify reports irrelevant warnings.
            }
          }),
          // new webpack.optimize.CommonsChunkPlugin({
          //   name: 'vendor' // Specify the common bundle's name.
          // }),
          webpackIsomorphicToolsPlugin,
          new CopyWebpackPlugin([{
            from: './src/common/app/favicons/',
            to: 'favicons'
          }], {
            ignore: ['original/**']
          })
        );
      }
      return plugins;
    })(),
    performance: {
      // hints: process.env.NODE_ENV === 'production' ? 'warning' : false,
      hints: false
    },
    resolve: {
      extensions: ['.js'], // .json is ommited to ignore ./firebase.json
      modules: [`${constants.ABSOLUTE_BASE}/src`, 'node_modules'],
      alias: {
        react$: require.resolve(path.join(constants.NODE_MODULES_DIR, 'react'))
      }
    }
  };

  return config;
}
