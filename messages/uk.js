/* eslint-disable max-len, quote-props, quotes */
export default [
  {
    "id": "address.full.apartment",
    "defaultMessage": "кв."
  },
  {
    "id": "address.welcome.form.find",
    "defaultMessage": "Знайти"
  },
  {
    "id": "address.welcome.form.label",
    "defaultMessage": "Ваша вулиця і номер будинку"
  },
  {
    "id": "address.welcome.placeholder",
    "defaultMessage": "Хрещатик 37"
  },
  {
    "id": "address.welcome.title1",
    "defaultMessage": "З нами легше працювати"
  },
  {
    "id": "address.welcome.title2",
    "defaultMessage": "Обіди - Dostolu"
  },
  {
    "id": "basket.button.toCheckout",
    "defaultMessage": "Оформити замовлення"
  },
  {
    "id": "basket.items.empty.title",
    "defaultMessage": "Тут мала б бути ваша вечеря"
  },
  {
    "id": "basket.total.below.minimum",
    "defaultMessage": "На жаль, ми не можемо прийняти Ваше замовлення, так як остаточна сума нижче мінімально необхідної."
  },
  {
    "id": "basket.total.sum",
    "defaultMessage": "Сума"
  },
  {
    "id": "basket.your.basket",
    "defaultMessage": "Ваше замовлення"
  },
  {
    "id": "cards.text.1",
    "defaultMessage": "Знайдіть ресторани, що доставляють вам їжу, ввівши свою адресу"
  },
  {
    "id": "cards.text.2",
    "defaultMessage": "Отримайте сотні меню, щоб вибрати їжу що вам досмаку"
  },
  {
    "id": "cards.text.3",
    "defaultMessage": "Оплатіть швидко і безпечно онлайн"
  },
  {
    "id": "cards.text.4",
    "defaultMessage": "Їжа готова і доставлена до ваших дверей"
  },
  {
    "id": "cards.title.1",
    "defaultMessage": "1. Знайдіть"
  },
  {
    "id": "cards.title.2",
    "defaultMessage": "2. Виберіть"
  },
  {
    "id": "cards.title.3",
    "defaultMessage": "3. Сплатіть"
  },
  {
    "id": "cards.title.4",
    "defaultMessage": "4. Насолоджуйтесь"
  },
  {
    "id": "checkout.button.complete",
    "defaultMessage": "Замовити"
  },
  {
    "id": "checkout.customer.email",
    "defaultMessage": "Адреса електронної пошти"
  },
  {
    "id": "checkout.customer.label",
    "defaultMessage": "Контактна інформація"
  },
  {
    "id": "checkout.customer.name",
    "defaultMessage": "Ваше ім'я та прізвище"
  },
  {
    "id": "checkout.customer.phone",
    "defaultMessage": "Номер телефону"
  },
  {
    "id": "checkout.delivery.address",
    "defaultMessage": "Адреса доставки"
  },
  {
    "id": "checkout.delivery.apartment",
    "defaultMessage": "Номер квартири"
  },
  {
    "id": "checkout.delivery.estimations",
    "defaultMessage": "Приблизний час доставки:"
  },
  {
    "id": "checkout.delivery.instructions",
    "defaultMessage": "Додаткові інструкції щодо доставки"
  },
  {
    "id": "checkout.delivery.no.slots",
    "defaultMessage": "На жаль цей ресторан вже закрито. Будь ласка, спробуйте вибрати інший ресторан на сторінці пошуку."
  },
  {
    "id": "checkout.delivery.time.asap",
    "defaultMessage": "Якомога швидше"
  },
  {
    "id": "checkout.delivery.time.custom",
    "defaultMessage": "Виберіть час"
  },
  {
    "id": "checkout.delivery.time.target",
    "defaultMessage": "Час доставки"
  },
  {
    "id": "checkout.payment.option.cash",
    "defaultMessage": "Готівка"
  },
  {
    "id": "checkout.payment.option.cash.notes",
    "defaultMessage": "Будь ласка, оплатіть готівкою при отриманні замовлення."
  },
  {
    "id": "checkout.payment.options",
    "defaultMessage": "Способи оплати:"
  },
  {
    "id": "header.main.title",
    "defaultMessage": "dostolu online"
  },
  {
    "id": "home.card.text.1",
    "defaultMessage": "Dostolu.Online – одна з кращих служб доставки їжі по всій Україні. Замовлення виконується легко, надійно і проходить протягом декількох секунд. Вибирайте Українські страви, Бургери, Піцу, Фаст-фуд, Європейську кухню, Страви Азії, Суші, Східні страви з ресторанів які завжди поруч: легко та зручним для вас способом виберіть потрібну вам страву в інтерактивному режимі. Отримайте знижки при оплаті картками MasterCard та Visa."
  },
  {
    "id": "home.card.text.2",
    "defaultMessage": "Dostolu.Online – це швидка доставка їжі з найближчих до Вас ресторанів. Наш сервіс орієнтований на клієнтів, що цінують швидкість доставки. З ресторанами-партнерами, що додаються щодня Ви зможете відслідкувати топ-рейтінг ресторанів у Вашому районі. Все що вам потрібно зробити - введіть адресу куди потрібно доставити їжу і ми подбаємо про все інше. All you have to D.O.? Love Dostolu.Online."
  },
  {
    "id": "home.card.title.1",
    "defaultMessage": "Миттєве замовлення їжі"
  },
  {
    "id": "home.card.title.2",
    "defaultMessage": "Доставка їжі для всіх"
  },
  {
    "id": "not.found.back",
    "defaultMessage": "Назад"
  },
  {
    "id": "not.found.message",
    "defaultMessage": "Здається цю сторінку хтось з'їв.\nСпробуйте ще раз!"
  },
  {
    "id": "property.time.closed",
    "defaultMessage": "Закрито сьогодні"
  },
  {
    "id": "restaurants.list.warning.offline",
    "defaultMessage": "На жаль, ці ресторани наразі закрито."
  },
  {
    "id": "restaurants.no.service.back",
    "defaultMessage": "Назад до пошуку"
  },
  {
    "id": "restaurants.no.service.message",
    "defaultMessage": "Трапилось так, що ми не змогли щось для Вас знайти."
  },
  {
    "id": "submit.basket.total.sum",
    "defaultMessage": "Сума"
  },
  {
    "id": "submit.basket.your.order",
    "defaultMessage": "Ваше замовлення"
  },
  {
    "id": "submit.delivery.details.header",
    "defaultMessage": "Деталі доставки"
  },
  {
    "id": "submit.order.details.header",
    "defaultMessage": "Деталі замовлення"
  },
  {
    "id": "submit.order.reference",
    "defaultMessage": "Номер Вашого замовлення:"
  },
  {
    "id": "submit.status.submitted",
    "defaultMessage": "Супер! Ваше замовлення було оформлено!"
  },
  {
    "id": "ui.property.orderMinimum",
    "defaultMessage": "Мін."
  },
  {
    "id": "ui.property.payment",
    "defaultMessage": "Тип оплати"
  }
];
