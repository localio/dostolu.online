/* eslint-disable max-len, quote-props, quotes */
export default [
  {
    "id": "address.full.apartment",
    "defaultMessage": "ap."
  },
  {
    "id": "address.welcome.form.find",
    "defaultMessage": "Find"
  },
  {
    "id": "address.welcome.form.label",
    "defaultMessage": "Your street name and house number"
  },
  {
    "id": "address.welcome.placeholder",
    "defaultMessage": "Dragomanova 33/44"
  },
  {
    "id": "address.welcome.title1",
    "defaultMessage": "The easy way"
  },
  {
    "id": "address.welcome.title2",
    "defaultMessage": "To takeaway"
  },
  {
    "id": "basket.button.toCheckout",
    "defaultMessage": "Proceed to checkout"
  },
  {
    "id": "basket.items.empty.title",
    "defaultMessage": "Here supposed to be your meal"
  },
  {
    "id": "basket.total.below.minimum",
    "defaultMessage": "Sorry, can not proceed: order is below minimum order value."
  },
  {
    "id": "basket.total.sum",
    "defaultMessage": "Sum"
  },
  {
    "id": "basket.your.basket",
    "defaultMessage": "Your Basket"
  },
  {
    "id": "cards.text.1",
    "defaultMessage": "Find restaurants that deliver to you by entering your address"
  },
  {
    "id": "cards.text.2",
    "defaultMessage": "Browse hundreds of menus to find the food you like"
  },
  {
    "id": "cards.text.3",
    "defaultMessage": "Pay fast & secure online or on delivery"
  },
  {
    "id": "cards.text.4",
    "defaultMessage": "Food is prepared & delivered to your door"
  },
  {
    "id": "cards.title.1",
    "defaultMessage": "1. Search"
  },
  {
    "id": "cards.title.2",
    "defaultMessage": "2. Choose"
  },
  {
    "id": "cards.title.3",
    "defaultMessage": "3. Pay"
  },
  {
    "id": "cards.title.4",
    "defaultMessage": "4. Enjoy"
  },
  {
    "id": "checkout.button.complete",
    "defaultMessage": "Place my order"
  },
  {
    "id": "checkout.customer.email",
    "defaultMessage": "Email address"
  },
  {
    "id": "checkout.customer.label",
    "defaultMessage": "Contact information"
  },
  {
    "id": "checkout.customer.name",
    "defaultMessage": "Full name"
  },
  {
    "id": "checkout.customer.phone",
    "defaultMessage": "Phone number"
  },
  {
    "id": "checkout.delivery.address",
    "defaultMessage": "Delivery address"
  },
  {
    "id": "checkout.delivery.apartment",
    "defaultMessage": "Apartments number"
  },
  {
    "id": "checkout.delivery.estimations",
    "defaultMessage": "Estimated delivery time:"
  },
  {
    "id": "checkout.delivery.instructions",
    "defaultMessage": "Delivery instructions"
  },
  {
    "id": "checkout.delivery.no.slots",
    "defaultMessage": "We are sorry, but this restaurant doesn't delivery any more today to your location. Please try to choose other restaurant."
  },
  {
    "id": "checkout.delivery.time.asap",
    "defaultMessage": "ASAP"
  },
  {
    "id": "checkout.delivery.time.custom",
    "defaultMessage": "Choose time"
  },
  {
    "id": "checkout.delivery.time.target",
    "defaultMessage": "Delivery time"
  },
  {
    "id": "checkout.payment.option.cash",
    "defaultMessage": "Cash"
  },
  {
    "id": "checkout.payment.option.cash.notes",
    "defaultMessage": "Please pay with cash upon receiving your order."
  },
  {
    "id": "checkout.payment.options",
    "defaultMessage": "Payment options:"
  },
  {
    "id": "header.main.title",
    "defaultMessage": "dostolu"
  },
  {
    "id": "home.card.text.1",
    "defaultMessage": "text 1"
  },
  {
    "id": "home.card.text.2",
    "defaultMessage": "text 2"
  },
  {
    "id": "home.card.title.1",
    "defaultMessage": "header 1"
  },
  {
    "id": "home.card.title.2",
    "defaultMessage": "header 2"
  },
  {
    "id": "not.found.back",
    "defaultMessage": "Bring me back"
  },
  {
    "id": "not.found.message",
    "defaultMessage": "It feels like this page has been eaten.\nPlease try again!"
  },
  {
    "id": "property.time.closed",
    "defaultMessage": "Closed today"
  },
  {
    "id": "restaurants.list.warning.offline",
    "defaultMessage": "Sorry, but the following restaurants are temporarily offline."
  },
  {
    "id": "restaurants.no.service.back",
    "defaultMessage": "Bring me back"
  },
  {
    "id": "restaurants.no.service.message",
    "defaultMessage": "It feels like we got nothing for you today. We are sorry."
  },
  {
    "id": "submit.basket.total.sum",
    "defaultMessage": "Sum"
  },
  {
    "id": "submit.basket.your.order",
    "defaultMessage": "Your Order"
  },
  {
    "id": "submit.delivery.details.header",
    "defaultMessage": "Delivery details"
  },
  {
    "id": "submit.order.details.header",
    "defaultMessage": "Order details"
  },
  {
    "id": "submit.order.reference",
    "defaultMessage": "Your order reference is:"
  },
  {
    "id": "submit.status.submitted",
    "defaultMessage": "Super! Your order has been placed!"
  },
  {
    "id": "ui.property.orderMinimum",
    "defaultMessage": "Min."
  },
  {
    "id": "ui.property.payment",
    "defaultMessage": "Payment type"
  }
];
