#!/usr/bin/env bash

VERSION=`cat package.json | jq -r '.version'`
NAME=`cat package.json | jq -r '.name'`
CONTAINER_IMAGE=gcr.io/$PROJECT_NAME/${NAME}:v${VERSION}
NAMESPACE=dostolu
CLUSTER=$PROJECT_CLUSTER
PORT=8000

# Prepare output file
IF=cloud-config.yaml
OF=cloud-config-ready.yaml
eval "echo \"$(cat ${IF})\" > ${OF}"
# Build image

rm -rf ./build

docker build -t ${CONTAINER_IMAGE} .
sudo /opt/google-cloud-sdk/bin/gcloud docker -- push ${CONTAINER_IMAGE}

sudo /opt/google-cloud-sdk/bin/kubectl apply --record --namespace=${NAMESPACE} -f ${OF}
# --context=${CLUSTER}

# Remove config file
rm ${OF}


