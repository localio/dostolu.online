FROM node:6

WORKDIR /app
COPY package.json /app/
RUN npm install
ADD . /app
RUN npm run clean
RUN npm run build --production

CMD ["npm", "run", "start"]
EXPOSE 8000
