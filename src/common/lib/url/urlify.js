import queryString from 'querystring';

/**
 * @return {String}
 */
export default (obj) => {
  const plain = {};
  Object.keys(obj).forEach(key => {
    if (typeof obj[key] === 'object') {
      Object.keys(obj[key]).forEach(levelKey => {
        plain[`${key}[${levelKey}]`] = obj[key][levelKey];
      });
    } else {
      plain[key] = obj[key];
    }
  });
  return queryString.stringify(plain);
};
