import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';
import connect from 'react-redux/lib/components/connect';

const getStyles = hasMore => ({
  loading: {
    textAlign: 'center',
    display: hasMore ? 'block' : 'none'
  }
});

const createClass = (Wrapped, action) =>
  class Pagination extends Component {
    // Passed via react-router or can be passed manually in React Native.
    static propTypes = {
      location: PropTypes.object,
      params: PropTypes.object,
      bottom: PropTypes.bool,
      pagination: PropTypes.object
    };

    static contextTypes = {
      store: PropTypes.object // Redux store.
    };

    constructor(props) {
      super(props);
      this.getNextPage = this.getNextPage.bind(this);
      this.state = { process: false };
    }

    componentWillReceiveProps(props) {
      const { pagination } = this.props;
      const { process } = this.state;
      if (process === false && pagination && props.bottom === true) {
        if (pagination.get('next')) {
          if (props.pagination.get('next') !== pagination.get('self')) {
            this.setState({ process: true });
            this.getNextPage();
          }
        }
      }
    }

    async getNextPage() {
      const { store: { dispatch } } = this.context;
      await dispatch(action(this.props));
      this.setState({ process: false });
    }

    render() {
      const { pagination, ...other } = this.props;
      const hasMore = pagination && pagination.get('next');
      const styles = getStyles(hasMore);
      return (
        <div>
          <Wrapped {...other} />
          {hasMore && (
            <div style={styles.loading}>
              Loading ...
            </div>
          )}
        </div>
      );
    }
  };

export default function pagination(action, storeName) {
  return Wrapped => connect(state => ({
    bottom: state.ui.bottom,
    pagination: state[storeName].pagination
  }))(createClass(Wrapped, action));
}
