import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';

export default function fetch(...actions) {
  return Wrapped => class Fetch extends Component {

    static contextTypes = {
      store: PropTypes.object // Redux store.
    };

    // Passed via react-router or can be passed manually in React Native.
    static propTypes = {
      location: PropTypes.object,
      params: PropTypes.object
    };

    // For server side fetching. Check frontend/render.js
    static fetchActions = actions;

    // For client side fetching.
    async componentDidMount() {
      const { store: { dispatch } } = this.context;
      // const { location, params } = this.props;
      actions.forEach(action => {
        if (action instanceof Array) {
          return action.reduce(
            (prev, curr) => prev.then(() => dispatch(curr(this.props))),
            Promise.resolve()
          );
        }
        return dispatch(action(this.props));
      });
    }

    render() {
      return <Wrapped {...this.props} />;
    }

  };
}
