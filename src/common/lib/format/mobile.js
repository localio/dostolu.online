const formatPhoneNumber = number => {
  // Cast to string
  number = number.toString();
  let res = '';

  // deaf proxy-phone number. eg 1800201614298015
  res = number.match(/^(1800(?:1|2))0(\d{8,})$/);
  if (res) {
    return `${res[1]} ${formatPhoneNumber(res[2])}`;
  }
  // 11x xxxxxxx
  res = number.match(/^(11\d)(\d{7})$/);
  if (res) {
    return `0${res[1]} ${res[2]}`;
  }
  // 1x1 xxx xxxx (eg. 0141 987 6543)
  res = number.match(/^(1\d1)(\d{3})(\d{4})$/);
  if (res) {
    return `0${res[1]} ${res[2]} ${res[3]}`;
  }
  // 2x xxxx xxxx (eg. 020 7386 7788)
  res = number.match(/^(2\d)(\d{4})(\d{4})$/);
  if (res) {
    return `0${res[1]} ${res[2]} ${res[3]}`;
  }
  // 2x xxxx xxxx (eg. 020 7386 7788)
  res = number.match(/^(3\d)(\d{4})(\d{4})$/);
  if (res) {
    return `0${res[1]} ${res[2]} ${res[3]}`;
  }
  // 1xxx xxxxxx
  res = number.match(/^(1\d{3})(\d{6})$/);
  if (res) {
    return `0${res[1]} ${res[2]}`;
  }
  // 1xxxxxxxx
  res = number.match(/^(1\d{8})$/);
  if (res) {
    return `0${res[1]}`;
  }
  // 5x xxxx xxxx
  res = number.match(/^(5\d)(\d{4})(\d{4})$/);
  if (res) {
    return `0${res[1]} ${res[2]} ${res[3]}`;
  }
  // 7xxx xxxxxx
  res = number.match(/^(7\d{3})(\d{6})$/);
  if (res) {
    return `0${res[1]} ${res[2]}`;
  }
  // 8xx xxx xxxx
  res = number.match(/^(8\d{2})(\d{3})(\d{4})$/);
  if (res) {
    return `0${res[1]} ${res[2]} ${res[3]}`;
  }
  // 9xxx xxxxxx
  res = number.match(/^(9\d{3})(\d{6})$/);
  if (res) {
    return `0${res[1]} ${res[2]}`;
  }
  return number;
};

export default formatPhoneNumber;
