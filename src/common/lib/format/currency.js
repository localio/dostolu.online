export default amount => {
  let amountNumber = parseInt(amount, 10);
  if (Number.isNaN(amountNumber)) {
    amountNumber = 0;
  }
  return `£${(amountNumber / 100).toFixed(2)}`;
};
