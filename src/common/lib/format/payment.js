const payments = {
  cash: 'Cash',
  online: 'Prepaid online'
};

export default payment => payments[payment.toLowerCase()];
