import { expect } from 'chai';
import format from '../mobile';

/**
 * result -> expected value
 * @type {*[]}
 */
const fixture = [
  // deaf proxy-phone number
  // ['1800201614298015', '?'],

  // 11x xxxxxxx
  ['1129012343', '0112 9012343'],

  // 1x1 xxx xxxx (eg. 0141 987 6543)
  ['1419876543', '0141 987 6543'],

  // 2x xxxx xxxx (eg. 020 7386 7788)
  ['2073867788', '020 7386 7788'],

  // 2x xxxx xxxx (eg. 020 7386 7788)
  ['2073867788', '020 7386 7788'],

  // 1xxx xxxxxx
  ['1234789123', '01234 789123'],

  // 1xxxxxxxx
  ['112345678', '0112345678'],

  // 5x xxxx xxxx
  ['5243346545', '052 4334 6545'],

  // 7xxx xxxxxx
  ['7123123456', '07123 123456'],

  // 8xx xxx xxxx
  ['8901231234', '0890 123 1234'],

  // 9xxx xxxxxx
  ['9012123456', '09012 123456'],

  // Other
  ['not a phone number', 'not a phone number'],

  // Work with numbers
  [9012123456, '09012 123456']
];

describe('format mobile', () => {
  it('Should properly format phone numbers', () => {
    fixture.map(fixtureData =>
      expect(format(fixtureData[0])).to.be.equal(fixtureData[1])
    );
  });
});
