import { expect } from 'chai';
import currency from '../currency';

const fixture = {
  123: '£1.23',
  1000: '£10.00',
  '': '£0.00',
  a: '£0.00'
};

describe('formatCurrency', () => {
  it('should format currency', () => {
    Object.keys(fixture).forEach(key => {
      expect(currency(key)).to.be.equal(fixture[key]);
    });
  });
});
