import './App.scss';
import Helmet from 'react-helmet';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';
import favicon from '../../common/app/favicon';
import start from '../../redux/app/start';
import connect from 'react-redux/lib/components/connect';
import { locationShape } from 'react-router/lib/PropTypes';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Page from '../common/components/ui/Page/Page.react';
import Footer from '../common/components/ui/Footer/Footer.react';
import lockScreen from '../common/lib/lockScreen';

injectTapEventPlugin();

// v4-alpha.getbootstrap.com/getting-started/introduction/#starter-template
const bootstrap4Metas = [
  { charset: 'utf-8' },
  {
    name: 'viewport',
    content: 'width=device-width, initial-scale=1, shrink-to-fit=no'
  }, {
    'http-equiv': 'x-ua-compatible',
    content: 'ie=edge'
  }, {
    name: 'mobile-web-app-capable',
    content: 'yes'
  }
];

class App extends Component {

  static propTypes = {
    children: PropTypes.object.isRequired,
    currentLocale: PropTypes.string.isRequired,
    location: locationShape,
    fixedBody: PropTypes.bool
  };

  render() {
    const { children, currentLocale, fixedBody } = this.props;
    if (process.env.IS_BROWSER) {
      lockScreen(fixedBody);
    }
    return (
      <div>
        <Helmet
          htmlAttributes={{ lang: currentLocale }}
          meta={[
            ...bootstrap4Metas,
            {
              name: 'description',
              content: ''
            },
            ...favicon.meta
          ]}
          link={[
            ...favicon.link
          ]}
        />
        <Page>
          {children}
        </Page>
        <Footer />
      </div>
    );
  }

}

App = start(App);

export default connect(state => ({
  currentLocale: state.intl.currentLocale,
  fixedBody: state.ui.fixedBody
}))(App);
