// A higher order component for loading.
import Loading from '../components/ui/Loading/Loading.react';
// @todo: weak point, should be common for all parts of web apps (?)
import NotFound from '../../webapp/pages/notFound/NotFound.react';
import React from 'react';
import Component from 'react/lib/ReactComponent';

export default function loading(Wrapped, propsToCheck, options = {}) {
  const {
    customLoading,
    isCollection = false
  } = options;

  return class Wrapper extends Component {

    render() {
      const props = propsToCheck.map(prop => this.props[prop]);
      // Null is evidence of absence.
      if (!isCollection && props.some(prop => prop === undefined)) {
        return <NotFound />;
      }
      if (props.some(prop => prop === null)) {
        return <Loading />;
      }
      // For better loading granularity.
      if (customLoading && customLoading(this.props)) {
        return <Loading />;
      }
      return <Wrapped {...this.props} />;
    }

  };
}
