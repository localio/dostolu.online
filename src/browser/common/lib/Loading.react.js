// Text loading component with two important limits.
// https://www.nngroup.com/articles/response-times-3-important-limits
// Example:
// {!users ?
//   <Loading />
// :
//   <div>
//     users here
//   </div>
// }
// TODO: Make it universal.
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Component from 'react/lib/ReactComponent';

export default class Loading extends Component {

  static propTypes = {
    loadingText: PropTypes.string.isRequired,
    longLoadingText: PropTypes.string.isRequired
  };

  static defaultProps = {
    loadingText: 'Loading',
    longLoadingText: 'Still loading, please check your connection'
  };

  constructor(props) {
    super(props);
    this.state = {
      // Render no-break space for the first second of loading.
      currentText: String.fromCharCode(160)
    };
  }

  componentDidMount() {
    this.timer = setTimeout(() => {
      this.setState({ currentText: this.props.loadingText });
    }, 1000);
    this.longTimer = setTimeout(() => {
      this.setState({ currentText: this.props.longLoadingText });
    }, 10000);
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
    clearTimeout(this.longTimer);
  }

  render() {
    const progressStyle = {
      textAlign: 'center',
      padding: '16px'
    };
    const progressTextStyle = {
      padding: '16px'
    };

    return (
      <div style={progressStyle}>
        <p style={progressTextStyle}>{this.state.currentText}</p>
      </div>
    );
  }

}
