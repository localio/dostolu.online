import lockScreen from './lockScreen';
import { expect } from 'chai';

/**
 * Get window mock object
 */
function getWindowMock() {
  return {
    document: {
      documentElement: {
        scrollTop: 0
      },
      body: {
        style: {
          top: 0,
          overflowY: 0,
          position: ''
        }
      }
    }
  };
}

describe('lockScreen', () => {
  it('do nothing if there is no window object', () => {
    const w = false;
    lockScreen(true, w);
    expect(w).to.be.false;
  });

  it('will change body style when flag is true with no scrollbar', () => {
    const w = getWindowMock();
    // change windown scroll top
    lockScreen(true, w);
    expect(w.document.body.style.position).to.be.equal('fixed');
    expect(w.document.body.style.overflowY).to.be.null;
    expect(w.document.body.style.top).to.be.equal('0px');
  });

  it('will change body style when flag is true with scrollbar', () => {
    const w = getWindowMock();
    // change windown scroll top
    w.document.documentElement.scrollTop = 145;
    lockScreen(true, w);
    expect(w.document.body.style.position).to.be.equal('fixed');
    expect(w.document.body.style.overflowY).to.be.equal('scroll');
    expect(w.document.body.style.top).to.be.equal('-145px');
  });

  it('will release body style when flag is false after lock', () => {
    const w = getWindowMock();
    // change windown scroll top
    w.document.documentElement.scrollTop = 100;
    w.document.body.style.position = 'fixed';
    w.document.body.style.overflowY = 'scroll';
    w.document.body.style.top = '-145px';
    lockScreen(false, w);
    expect(w.document.body.style.position).to.be.equal('static');
    expect(w.document.body.style.overflowY).to.be.equal('auto');
    expect(w.document.body.style.top).to.be.equal('0px');
  });

  it('will prevent body style when flag is false with default values', () => {
    const w = getWindowMock();
    // change windown scroll top
    w.document.documentElement.scrollTop = 100;
    lockScreen(false, w);
    expect(w.document.body.style.position).to.be.equal('');
    expect(w.document.body.style.overflowY).to.be.equal(0);
    expect(w.document.body.style.top).to.be.equal(0);
  });
});
