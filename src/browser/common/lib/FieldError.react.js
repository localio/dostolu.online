import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Component from 'react/lib/ReactComponent';
import ValidationError from '../../../redux/lib/validation/ValidationError';


export default class FieldError extends Component {

  static propTypes = {
    error: PropTypes.any,
    prop: PropTypes.string.isRequired
  };

  formatError(error) {
    return `
      ${error.description || 'Please enter a description.'}
      ${error.email || 'Please enter an email address.'}
      ${error.name || 'Please enter a name.'}
      ${error.password || 'Please enter a password.'}
      ${error.other || "You can't leave this empty."}
    `;
  }
  render() {
    const { error, prop } = this.props;
    if (!(error instanceof ValidationError)) return null;
    if (error.params.prop !== prop) return null;

    return (
      <div className="alert alert-danger" role="alert">
        formatError(error.params)
      </div>
    );
  }

}
