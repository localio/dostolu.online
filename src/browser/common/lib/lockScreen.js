export default (flag, w = window) => {
  // Easier to unittest without global browser check
  if (w) {
    const { document: { body, documentElement: html } } = w;

    if (flag) {
      // Block scrolling
      // Make body fixed and move it up by scroll top
      // So scroll will be still visible, but it will be impossible to
      // scroll the window
      const scrollTop = html.scrollTop || body.scrollTop || 0;
      body.style.top = `${-scrollTop}px`;
      body.style.overflowY = scrollTop && 'scroll' || null;
      body.style.position = 'fixed';
      body.style.width = '100%';
    } else if (body.style.position === 'fixed') {
      // Unblock scrolling
      const top = body.style.top && body.style.top.replace(/[^0-9.]*/ig, '');
      body.style.position = 'static';
      body.style.top = '0px';
      body.style.overflowY = 'auto';
      html.scrollTop = body.scrollTop = top;
      body.style.width = 'auto';
    }
  }
};
