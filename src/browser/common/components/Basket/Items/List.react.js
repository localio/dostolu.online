import './List.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import BasketItemsItem from './Item.react';
import BasketItemEmpty from './Empty.react';

const BasketItemsList = (props) => {
  const { basket } = props;

  if (!basket || basket.get('count') <= 0) {
    return <BasketItemEmpty />;
  }

  return (
    <ul className="basket-items-list">
      {basket.get('items').valueSeq().map(item =>
        <BasketItemsItem
          key={`basket_${item.get('_id')}`}
          id={item.get('_id')}
          quantity={item.get('quantity')}
          restaurantId={basket.get('restaurant')}
          menuId={basket.get('menu')}
        />
      )}
    </ul>
  );
};

BasketItemsList.propTypes = {
  basket: PropTypes.object
};

export default BasketItemsList;
