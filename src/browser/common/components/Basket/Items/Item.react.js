import './Item.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import connect from 'react-redux/lib/components/connect';
import Intl from '../../ui/Intl/Intl.react';
import Price from '../../ui/Price/Price.react';
import * as basketActions from '../../../../../redux/basket/actions';

const BasketItemsItem = (props) => {
  const { id, quantity, menuItems } = props;
  if (!menuItems) {
    return <li></li>;
  }
  const menuItem = menuItems.get(id);
  const name = menuItem.get('name');
  const price = quantity * (menuItem.get('price'));

  const addItem = (e) => {
    e.preventDefault();
    const { addItem, menuId, restaurantId } = props;
    addItem(id, menuItem.get('price'), menuId, restaurantId);
  };

  const removeItem = (e) => {
    e.preventDefault();
    const { removeItem, restaurantId } = props;
    removeItem(id, menuItem.get('price'), restaurantId);
  };

  return (
    <li className="basket-items-item">
      <span className="basket-items-item__quantity">
        <a href="" onClick={removeItem}>-</a>
        <span>{quantity}</span>
        <a href="" onClick={addItem}>+</a>
      </span>
      <Intl className="basket-items-item__name" text={name} />
      <Price className="basket-items-item__price price" price={price} />
    </li>
  );
};

BasketItemsItem.propTypes = {
  id: PropTypes.string.isRequired,
  quantity: PropTypes.number.isRequired,
  menuId: PropTypes.string.isRequired,
  restaurantId: PropTypes.string.isRequired,
  menuItems: PropTypes.object,
  addItem: PropTypes.func,
  removeItem: PropTypes.func
};

export default connect(state => ({
  menuItems: state.menu.menuItems
}), basketActions)(BasketItemsItem);
