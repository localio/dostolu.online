import './Empty.scss';
import React from 'react';
import BasketIconEmpty from '../Icon/Empty/Empty.react';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  emptybasketTitle: {
    defaultMessage: 'Here supposed to be your meal',
    id: 'basket.items.empty.title'
  }
});

const BasketItemsEmpty = () =>
  <div className="basket-items-empty">
    <section>
      <p><FormattedMessage {..._messages.emptybasketTitle} /></p>
      <BasketIconEmpty />
    </section>
  </div>;

export default BasketItemsEmpty;
