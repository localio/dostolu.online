import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Icon from '../../../ui/Icon/Icon.react';

const BasketIconEmpty = (props) =>
  <Icon icon="cutlery" className={props.className} />;

BasketIconEmpty.propTypes = {
  className: PropTypes.string
};

export default BasketIconEmpty;
