import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Icon from '../../../ui/Icon/Icon.react';

const BasketIconClose = (props) =>
  <Icon icon="close" className={props.className} />;

BasketIconClose.propTypes = {
  className: PropTypes.string
};

export default BasketIconClose;
