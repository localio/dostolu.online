import './Sum.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import { defineMessages, FormattedMessage } from 'react-intl';
import Price from '../../ui/Price/Price.react';

const _messages = defineMessages({
  sum: {
    defaultMessage: 'Sum',
    id: 'basket.total.sum'
  },
  belowMinimum: {
    defaultMessage: 'Sorry, can not proceed: order is below minimum order value.',
    id: 'basket.total.below.minimum'
  }
});

const BasketTotalSum = (props) => {
  const { basket, restaurant } = props;
  const isActive = basket && (restaurant.get('orderMinimum') <= basket.get('total'));
  const total = (basket && basket.get('total')) || 0;

  return (
    <section className="basket-total-sum">
      <dl>
        <dt>
          <FormattedMessage {..._messages.sum} />
        </dt>
        <dd>
          <Price className="basket-total-sum__price" price={total} />
        </dd>
      </dl>
      {basket && !isActive && <p className="error">
        <FormattedMessage {..._messages.belowMinimum} />
      </p>}
    </section>
  );
};

BasketTotalSum.propTypes = {
  basket: PropTypes.object,
  restaurant: PropTypes.object
};

export default BasketTotalSum;
