import './Button.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import connect from 'react-redux/lib/components/connect';
import classNames from 'classnames';
import { defineMessages, FormattedMessage } from 'react-intl';
import { push } from 'react-router-redux/lib/actions';
import * as uiActions from '../../../../../redux/ui/actions';
import * as menuUiActions from '../../../../../redux/menuUi/actions';

const _messages = defineMessages({
  proceedToCheckout: {
    defaultMessage: 'Proceed to checkout',
    id: 'basket.button.toCheckout'
  }
});

const BasketCheckoutButton = (props) => {
  const { basket, restaurant } = props;
  const isActive = basket && (restaurant.get('orderMinimum') <= basket.get('total'));
  const statusCss = {
    'action-button--inactive': !isActive,
    'action-button--active': isActive,
  };
  const desktopCss = classNames('action-button--desktop', statusCss);
  const mobileCss = classNames('action-button--mobile', statusCss);

  const toggleBasket = () => {
    const { toggleFixedBody, toggleMenuPage } = props;
    toggleFixedBody();
    toggleMenuPage();
  };

  const goToCheckoutMobile = () => {
    const { push } = props;
    toggleBasket();
    if (!isActive) return;
    const url = `/restaurant/${restaurant.get('slug')}/checkout`;
    push(url);
  };

  const goToCheckoutDesktop = () => {
    const { push } = props;
    if (!isActive) return;
    const url = `/restaurant/${restaurant.get('slug')}/checkout`;
    push(url);
  };

  return (
    <section className="basket-checkout-button">
      <button className={desktopCss} onClick={goToCheckoutDesktop}>
        <FormattedMessage {..._messages.proceedToCheckout} />
      </button>
      <button className={mobileCss} onClick={goToCheckoutMobile}>
        <FormattedMessage {..._messages.proceedToCheckout} />
      </button>
    </section>
  );
};

BasketCheckoutButton.propTypes = {
  /**
   * Basket object
   */
  basket: PropTypes.object,
  /**
   * Restaurant object
   */
  restaurant: PropTypes.object.isRequired,
  toggleFixedBody: PropTypes.func,
  toggleMenuPage: PropTypes.func,
  push: PropTypes.func
};

export default connect(
  null,
  { push, ...uiActions, ...menuUiActions }
)(BasketCheckoutButton);
