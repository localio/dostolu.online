import './Sticky.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import connect from 'react-redux/lib/components/connect';
import Price from '../../ui/Price/Price.react';
import IconsBasket from './Icons/Basket.react';
import * as menuUiActions from '../../../../../redux/menuUi/actions';
import * as uiActions from '../../../../../redux/ui/actions';

const BasketSticky = (props) => {
  const { restaurant, baskets } = props;
  let total = 0;
  let count = 0;
  // As we have no idea which restaurant is active - do not display basket at all
  if (!restaurant) {
    return <div></div>;
  }
  const restaurantId = restaurant.get('_id');
  if (baskets.get(restaurantId)) {
    total = baskets.getIn([restaurantId, 'total']);
    count = baskets.getIn([restaurantId, 'count']);
  }

  const toggleBasket = () => {
    const { toggleMenuPage, toggleFixedBody } = props;
    toggleMenuPage();
    toggleFixedBody();
  };

  return (
    <div className="basket-sticky" onClick={toggleBasket}>
      <span className="basket-sticky--count">{count}</span>
      <strong>
        <IconsBasket />
        <Price price={total} />
      </strong>
    </div>
  );
};

BasketSticky.propTypes = {
  restaurant: PropTypes.object,
  baskets: PropTypes.object,
  toggleMenuPage: PropTypes.func,
  toggleFixedBody: PropTypes.func
};

export default connect(state => ({
  restaurant: state.restaurant.restaurant,
  baskets: state.basket.map
}), { ...menuUiActions, ...uiActions })(BasketSticky);
