import './Basket.scss';
import React from 'react';
import Icon from '../../../ui/Icon/Icon.react';

export default () => <Icon icon="shopping-cart" className="basket-sticky-icons-basket" />;
