import './Basket.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import connect from 'react-redux/lib/components/connect';
import classNames from 'classnames';
import * as basketActions from '../../../../redux/basket/actions';
import * as uiActions from '../../../../redux/ui/actions';
import * as menuUiActions from '../../../../redux/menuUi/actions';
import { defineMessages, FormattedMessage } from 'react-intl';
import BasketItemList from './Items/List.react';
import BasketIconClose from './Icon/Close/Close.react';
import BasketTotalSum from './Total/Sum.react';
import BasketCheckoutButton from './Checkout/Button.react';

const _messages = defineMessages({
  yourBasket: {
    defaultMessage: 'Your Basket',
    id: 'basket.your.basket'
  }
});

// const getAllHeight = () => {
//   let height = 'auto';
//   if (window) {
//     height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
//     height = `${height}px`;
//   }
//   return height;
// };

const Basket = (props) => {
  const { baskets, restaurant, menuPage, checkoutButton, viewportHeight } = props;
  // Do not display basket as we don't know
  // To which restaurant it belongs
  if (!restaurant) {
    return <div></div>;
  }
  const restaurantId = restaurant.get('_id');
  // Get basket
  const basket = baskets.get(restaurantId);

  const css = classNames('basket', {
    'basket--open': menuPage
  });
  let styles = {};
  if (menuPage) {
    styles = {
      height: `${viewportHeight}px`
    };
  }

  const toggleBasket = () => {
    const { toggleFixedBody, toggleMenuPage } = props;
    toggleFixedBody();
    toggleMenuPage();
  };

  return (
    <section className={css} style={styles}>
      <header>
        <FormattedMessage {..._messages.yourBasket} />
        <div onClick={toggleBasket}>
          <BasketIconClose />
        </div>
      </header>
      <BasketItemList basket={basket} />
      <footer>
        <BasketTotalSum
          restaurant={restaurant}
          basket={basket}
        />
        {checkoutButton && <BasketCheckoutButton
          restaurant={restaurant}
          basket={basket}
        />}
      </footer>
    </section>
  );
};

Basket.propTypes = {
  baskets: PropTypes.object,
  restaurant: PropTypes.object,
  menuPage: PropTypes.bool,
  toggleFixedBody: PropTypes.func,
  toggleMenuPage: PropTypes.func,
  viewportHeight: PropTypes.number,
  /**
   * Show or skip "Go to checkout" page
   */
  checkoutButton: PropTypes.bool
};

export default connect(state => ({
  baskets: state.basket.map,
  restaurant: state.restaurant.restaurant,
  menuPage: state.menuUi.menuPage,
  viewportHeight: state.ui.viewportHeight
}), { ...basketActions, ...uiActions, ...menuUiActions })(Basket);
