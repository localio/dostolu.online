import './Card.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import CardIcon from '../Icon/Icon.react';

const Card = (props) => {
  const { icon, title, text } = props;
  return (
    <div className="card">
      <CardIcon icon={icon} />
      <div className="card-header">
        {title}
      </div>
      <div className="card-copy">
        <p>{text}</p>
      </div>
    </div>
  );
};

Card.propTypes = {
  /**
   * Card icon
   */
  icon: PropTypes.string.isRequired,
  /**
   * Card title
   */
  title: PropTypes.object.isRequired,
  /**
   * Card text
   */
  text: PropTypes.object.isRequired
};

export default Card;
