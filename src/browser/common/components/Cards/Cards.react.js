import './Cards.scss';
import React from 'react';
import { defineMessages, FormattedMessage } from 'react-intl';
import Card from './Card/Card.react';

const _messages = defineMessages({
  cardsTitle1: {
    defaultMessage: '1. Search',
    id: 'cards.title.1'
  },
  cardsText1: {
    defaultMessage: 'Find restaurants that deliver to you by entering your address',
    id: 'cards.text.1'
  },
  cardsTitle2: {
    defaultMessage: '2. Choose',
    id: 'cards.title.2'
  },
  cardsText2: {
    defaultMessage: 'Browse hundreds of menus to find the food you like',
    id: 'cards.text.2'
  },
  cardsTitle3: {
    defaultMessage: '3. Pay',
    id: 'cards.title.3'
  },
  cardsText3: {
    defaultMessage: 'Pay fast & secure online or on delivery',
    id: 'cards.text.3'
  },
  cardsTitle4: {
    defaultMessage: '4. Enjoy',
    id: 'cards.title.4'
  },
  cardsText4: {
    defaultMessage: 'Food is prepared & delivered to your door',
    id: 'cards.text.4'
  }
});

export default () => <div id="cards">
  <div className="page">
    <Card
      icon="map"
      title={<FormattedMessage {..._messages.cardsTitle1} />}
      text={<FormattedMessage {..._messages.cardsText1} />}
    />
    <Card
      icon="pizza"
      title={<FormattedMessage {..._messages.cardsTitle2} />}
      text={<FormattedMessage {..._messages.cardsText2} />}
    />
    <Card
      icon="credit-card-2"
      title={<FormattedMessage {..._messages.cardsTitle3} />}
      text={<FormattedMessage {..._messages.cardsText3} />}
    />
    <Card
      icon="delivery-truck-1"
      title={<FormattedMessage {..._messages.cardsTitle4} />}
      text={<FormattedMessage {..._messages.cardsText4} />}
    />
  </div>
</div>;
