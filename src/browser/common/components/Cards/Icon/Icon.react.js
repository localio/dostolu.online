import './Icon.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Icon from '../../ui/Icon/Icon.react';

const CardIcon = (props) => (
  <Icon icon={props.icon} className="card-ico" />
);

CardIcon.propTypes = {
  /**
   * Icon name
   */
  icon: PropTypes.string.isRequired
};

export default CardIcon;
