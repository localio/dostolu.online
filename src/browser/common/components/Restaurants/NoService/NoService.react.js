import './NoService.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import connect from 'react-redux/lib/components/connect';
import { replace } from 'react-router-redux/lib/actions';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  restaurantsNoServiceBack: {
    defaultMessage: 'Bring me back',
    id: 'restaurants.no.service.back'
  },
  restaurantsNoServiceMessage: {
    defaultMessage: 'It feels like we got nothing for you today. We are sorry.',
    id: 'restaurants.no.service.message'
  }
});

const NoServise = (props) => {
  const { replace } = props;

  const backToHome = (e) => {
    e.preventDefault();
    replace('');
  };

  return (
    <div className="restaurants-no-service">
      <div>
        <p>
          <FormattedMessage {..._messages.restaurantsNoServiceMessage} />
        </p>
        <button onClick={backToHome} className="action-button--active">
          <FormattedMessage {..._messages.restaurantsNoServiceBack} />
        </button>
      </div>
    </div>
  );
};

NoServise.propTypes = {
  /**
   * Change URL
   */
  replace: PropTypes.func.isRequired
};

export default connect(null, { replace })(NoServise);
