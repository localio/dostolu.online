import './List.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import connect from 'react-redux/lib/components/connect';
import Item from '../Item/Item.react';
import fetch from '../../../../../common/lib/fetch/fetch';
import * as restaurantsActions from '../../../../../redux/restaurants/actions';
import loading from '../../../lib/loading';
import NoService from '../NoService/NoService.react';
import { push } from 'react-router-redux/lib/actions';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  offlineWarning: {
    defaultMessage: 'Sorry, but the following restaurants are temporarily offline.',
    id: 'restaurants.list.warning.offline'
  }
});


let List = (props) => {
  const { online, offline } = props;

  // No results - no restaurants :-(
  if (online.size === 0 && offline.size === 0) {
    return <NoService />;
  }

  return (
    <div className="restaurants-list">
      <section>
        <ul>
          {online.valueSeq().map(result =>
            <li
              key={result.get('_id')}
            >
              <Item restaurant={result} />
            </li>
          )}
        </ul>
      </section>
      {offline.size > 0 && <section className="restaurants-list">
        <p><FormattedMessage {..._messages.offlineWarning} /></p>
        <ul>
          {offline.valueSeq().map(result =>
            <li
              key={result.get('_id')}
            >
              <Item restaurant={result} />
            </li>
          )}
        </ul>
      </section>}
    </div>
  );
};

List.propTypes = {
  /**
   * Search results (restaurants list)
   */
  online: PropTypes.object,
  offline: PropTypes.object
};

List = loading(List, ['online', 'offline'], { isCollection: true });

List = fetch(restaurantsActions.search)(List);

export default connect(state => ({
  online: state.restaurants.online,
  offline: state.restaurants.offline
}), { push, ...restaurantsActions })(List);
