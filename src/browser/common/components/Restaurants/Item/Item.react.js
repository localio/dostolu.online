import './Item.scss';
import PropertyTime from '../../ui/Property/Time.react';
import PropertyOrderMinimum from '../../ui/Property/OrderMinimum.react';
import Icon from '../../ui/Icon/Icon.react';
import Intl from '../../ui/Intl/Intl.react';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Link from 'react-router/lib/Link';
import { push } from 'react-router-redux/lib/actions';
import connect from 'react-redux/lib/components/connect';
import { prefillRestaurant } from '../../../../../redux/restaurant/actions';
import { cleanMenu } from '../../../../../redux/menu/actions';
import { isOffline } from '../../../../../redux/restaurant/lib/status';
import className from 'classnames';
const defaultRestaurant = require('./../../../../assets/images/default.png');

const Item = (props) => {
  const { restaurant } = props;
  const cuisineModel = restaurant.get('cuisineModel');
  // const address = restaurant.get('address');
  const url = `/restaurant/${restaurant.get('slug')}`;
  const css = className('restaurant-item', {
    'restaurant-item--offline': isOffline(restaurant.get('status'))
  });

  const logo = restaurant.get('logo') && restaurant.get('logo').length > 0
    ? restaurant.get('logo')
    : defaultRestaurant;

  const goToRestaurantPage = () => {
    const { push, prefillRestaurant, cleanMenu } = props;
    prefillRestaurant(restaurant);
    cleanMenu();
    push(url);
  };

  return (
    <section className={css} onClick={goToRestaurantPage}>
      <article>
        <figure>
          <img role="presentation" src={logo} />
        </figure>
        <header>
          <Link to={url}>
            <h3>
              <Intl text={restaurant.get('name')} />
            </h3>
          </Link>
          <span>{cuisineModel.get('name') && <Intl text={cuisineModel.get('name')} />}</span>

          {/* <address>Krakowskaja 24, kv 5</address> */}
        </header>
        <div className="restaurant-item-arrow-link">
          <Icon icon="arrow-right" />
        </div>
      </article>
      <summary>
        <ul>
          {/* <li> */}
            {/* <RestaurantsIcon icon="shopping-cart" /> */}
            {/* <span>Min. 18.00</span> */}
          {/* </li> */}
          {/* <li> */}
            {/* <RestaurantsIcon icon="delivery-truck-1" /> */}
            {/* <span>0.00</span> */}
          {/* </li> */}
          <li>
            <PropertyTime time={restaurant.get('openedToday')} />
          </li>
          {restaurant.get('orderMinimum') > 0 &&
            <li>
              <PropertyOrderMinimum minimum={restaurant.get('orderMinimum')} />
            </li>
          }
          {/* <li></li> */}
        </ul>
      </summary>
    </section>
  );
};

Item.propTypes = {
  /**
   * Restaurant Object
   * @var {RestaurantSearch}
   */
  restaurant: PropTypes.object.isRequired,
  push: PropTypes.func,
  prefillRestaurant: PropTypes.func,
  cleanMenu: PropTypes.func
};

export default connect(null, {
  push,
  prefillRestaurant,
  cleanMenu
})(Item);
