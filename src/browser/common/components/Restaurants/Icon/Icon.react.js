import './Icon.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Icon from '../../ui/Icon/Icon.react';

const RestaurantsIcon = (props) => (
  <Icon icon={props.icon} className="restaurants-ico" />
);

RestaurantsIcon.propTypes = {
  /**
   * Icon name
   */
  icon: PropTypes.string.isRequired
};

export default RestaurantsIcon;
