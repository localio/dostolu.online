import React from 'react';
import connect from 'react-redux/lib/components/connect';
import RestaurantsList from './List/List.react';
import loading from '../../lib/loading';

let Restaurants = () => <RestaurantsList />;

Restaurants = loading(Restaurants, ['currentAddress'], { isCollection: false });

export default connect(state => ({
  currentAddress: state.address.currentAddress
}))(Restaurants);
