import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Intl from '../../ui/Intl/Intl.react';

const AddressLine = (props) => {
  const { address } = props;
  if (!address) {
    return <div />;
  }
  const town = address.get('town');
  const district = address.get('district');
  const street = address.get('street');
  // const place = address.get('place');
  return (
    <address>
      <Intl text={town.get('name')} />,&nbsp;
      <Intl text={district.get('name')} />,&nbsp;
      <Intl text={street.get('name')} />&nbsp;
      {/* {place.get('housenumber')}*/}
    </address>
  );
};

AddressLine.propTypes = {
  /**
   * @var {Address}
   */
  address: PropTypes.object,
};

export default AddressLine;
