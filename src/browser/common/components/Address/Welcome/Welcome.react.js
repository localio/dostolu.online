import './Welcome.scss';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';
import * as addressActions from '../../../../../redux/address/actions';
import connect from 'react-redux/lib/components/connect';
import AddressSearch from '../Search/Search.react';
import { push } from 'react-router-redux/lib/actions';
import { defineMessages, FormattedMessage, intlShape, injectIntl } from 'react-intl';
import { getAddressUrl } from '../../../../../redux/address/lib';

const _messages = defineMessages({
  addressWelcomePlaceholder: {
    defaultMessage: 'Dragomanova 33/44',
    id: 'address.welcome.placeholder'
  },
  addressWelcomeFormLabel: {
    defaultMessage: 'Your street name and house number',
    id: 'address.welcome.form.label'
  },
  addressWelcomeFormFind: {
    defaultMessage: 'Find',
    id: 'address.welcome.form.find'
  },
  addressWelcomeTitle1: {
    defaultMessage: 'The easy way',
    id: 'address.welcome.title1'
  },
  addressWelcomeTitle2: {
    defaultMessage: 'To takeaway',
    id: 'address.welcome.title2'
  }
});

class AddressWelcome extends Component {

  static propTypes = {
    searchResults: PropTypes.object,
    currentAddress: PropTypes.object,
    findAddresses: PropTypes.func,
    clearAddresses: PropTypes.func,
    fetchAddress: PropTypes.func,
    setShortAddress: PropTypes.func,
    currentShortAddress: PropTypes.string,
    intl: intlShape.isRequired,
    push: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = { value: '' };
  }

  /**
   * Set address in the search string if it has been selected already
   */
  componentWillMount() {
    const { currentShortAddress } = this.props;
    if (currentShortAddress) {
      this.setState({
        value: currentShortAddress
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    const { currentShortAddress } = nextProps;
    const { value } = this.state;
    if (value === '' && currentShortAddress !== null) {
      this.setState({
        value: currentShortAddress
      });
    }
  }

  onSelect(address) {
    const { clearAddresses, fetchAddress, setShortAddress } = this.props;
    clearAddresses();
    this.setState({
      value: address
    });
    setShortAddress(address);
    fetchAddress();
  }

  onSubmit(event) {
    event.preventDefault();
    // Check if it is possible (is address actially loaded)
    const { currentAddress, push } = this.props;
    if (currentAddress) {
      const url = getAddressUrl(currentAddress);
      push(`/restaurants/${url}`);
    }
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
    const { findAddresses, clearAddresses } = this.props;
    const value = event.target.value;
    if (value.length > 2) {
      findAddresses(value);
    } else {
      clearAddresses();
    }
  }

  render() {
    const { searchResults, intl } = this.props;
    const placeholder = intl.formatMessage(_messages.addressWelcomePlaceholder);

    return (
      <section id="address">
        <div>
          <h1>
            <span>
              <strong>
                <FormattedMessage {..._messages.addressWelcomeTitle1} />
              </strong>
              <FormattedMessage {..._messages.addressWelcomeTitle2} />
            </span>
          </h1>
          <form onSubmit={this.onSubmit}>
            <label>
              <FormattedMessage {..._messages.addressWelcomeFormLabel} />
            </label>
            <section>
              <input
                type="text"
                autoComplete="off"
                name="address"
                placeholder={placeholder}
                value={this.state.value}
                onChange={this.handleChange}
              />
              {searchResults.size > 0 && <AddressSearch
                searchResults={searchResults}
                onSelect={this.onSelect}
              />}
              <button type="submit" className="action-button--active">
                <FormattedMessage {..._messages.addressWelcomeFormFind} />
              </button>
            </section>
          </form>
        </div>
      </section>
    );
  }
}

AddressWelcome = injectIntl(AddressWelcome);

export default connect(state => ({
  searchResults: state.address.searchResults,
  currentAddress: state.address.currentAddress,
  currentShortAddress: state.address.currentShortAddress
}), { push, ...addressActions })(AddressWelcome);
