import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Intl from '../../ui/Intl/Intl.react';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  apartmentNumber: {
    defaultMessage: 'ap.',
    id: 'address.full.apartment'
  }
});

const AddressFull = (props) => {
  const { address, apartment } = props;
  if (!address) {
    return <div />;
  }
  const town = address.get('town');
  const district = address.get('district');
  const street = address.get('street');
  const place = address.get('place');
  return (
    <address>
      <Intl text={town.get('name')} />,&nbsp;
      <Intl text={district.get('name')} />,&nbsp;
      <Intl text={street.get('name')} />&nbsp;
      {place.get('housenumber')}
      {apartment && <span>, <FormattedMessage {..._messages.apartmentNumber} /> {apartment}</span>}
    </address>
  );
};

AddressFull.propTypes = {
  /**
   * @var {Address}
   */
  address: PropTypes.object,
  apartment: PropTypes.string
};

export default AddressFull;
