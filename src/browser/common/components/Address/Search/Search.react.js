import './Search.scss';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';
import * as addressActions from '../../../../../redux/address/actions';
import * as locationActions from '../../../../../redux/location/actions';
import connect from 'react-redux/lib/components/connect';

class Search extends Component {

  static propTypes = {
    searchResults: PropTypes.object.isRequired,
    onSelect: PropTypes.func.isRequired,
    chooseAddress: PropTypes.func.isRequired,
    cleanLocation: PropTypes.func,
    setLocationPlace: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.onSelect = this.onSelect.bind(this);
  }

  async onSelect(event) {
    const { onSelect, chooseAddress, cleanLocation, setLocationPlace } = this.props;
    const id = event.target.getAttribute('data-id');
    const value = event.target.getAttribute('data-value');
    await chooseAddress(id);
    await cleanLocation();
    await setLocationPlace(id);
    onSelect(value);
  }

  render() {
    const { searchResults } = this.props;
    return (
      <div id="address-search-results">
        <ul>
          {searchResults.valueSeq().map(result =>
            <li
              key={result.get('_id')}
            >
              <span
                data-id={result.get('_id')}
                data-value={result.get('address')}
                onClick={this.onSelect}
              >{result.get('address')}</span>
            </li>
          )}
        </ul>
      </div>
    );
  }

}

export default connect(null, { ...addressActions, ...locationActions })(Search);
