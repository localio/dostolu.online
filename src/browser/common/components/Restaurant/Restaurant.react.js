import './Restaurant.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import connect from 'react-redux/lib/components/connect';
import Intl from '../ui/Intl/Intl.react';
import * as restaurantActions from '../../../../redux/restaurant/actions';
import fetch from '../../../../common/lib/fetch/fetch';
import PropertyAddress from '../ui/Property/Address.react';
import className from 'classnames';
import PropertyTime from '../ui/Property/Time.react';
import PropertyOrderMinimum from '../ui/Property/OrderMinimum.react';
import { isOffline } from '../../../../redux/restaurant/lib/status';

const defaultRestaurant = require('./../../../assets/images/default.png');

let Restaurant = (props) => {
  const { restaurant } = props;
  if (!restaurant) {
    return null;
  }

  const logo = restaurant.get('logo') && restaurant.get('logo').length > 0
    ? restaurant.get('logo')
    : defaultRestaurant;

  const css = className('restaurant', {
    'restaurant--offline': isOffline(restaurant.get('status'))
  });

  return (
    <div className={css}>
      <figure>
        <img role="presentation" src={logo} />
      </figure>
      <article>
        <h1>
          <Intl text={restaurant.get('name')} />
        </h1>
        <ul>
          <li>
            <PropertyAddress address={restaurant.get('addressModel')} />
          </li>
          <li>
            <PropertyTime time={restaurant.get('openedToday')} />
          </li>
          {restaurant.get('orderMinimum') > 0 &&
            <li>
              <PropertyOrderMinimum minimum={restaurant.get('orderMinimum')} />
            </li>
          }
        </ul>
      </article>
    </div>
  );
};

Restaurant.propTypes = {
  restaurant: PropTypes.object,
  params: PropTypes.object.isRequired
};

// Restaurant = loading(Restaurant, ['restaurant', 'restaurant']);

Restaurant = fetch(restaurantActions.fetchRestaurantBySlug)(Restaurant);

export default connect(state => ({
  restaurant: state.restaurant.restaurant
}))(Restaurant);
