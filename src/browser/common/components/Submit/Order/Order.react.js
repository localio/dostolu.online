import './Order.scss';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';
import fetch from '../../../../../common/lib/fetch/fetch';
import connect from 'react-redux/lib/components/connect';
import * as orderActions from '../../../../../redux/order/actions';
import loading from '../../../lib/loading';
import SubmitStatus from '../Status/Status.react';
import SubmitBasket from '../Basket/Basket.react';
import SubmitDeliveryDetails from '../DeliveryDetails/DeliveryDetails.react';
import SubmitOrderDetails from '../OrderDetails/OrderDetails.react';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  orderReference: {
    defaultMessage: 'Your order reference is: ',
    id: 'submit.order.reference'
  }
});

class SubmitOrder extends Component {
  static propTypes = {
    order: PropTypes.object
  };

  render() {
    const { order } = this.props;
    if (!order) {
      return <div />;
    }
    return (
      <section className="submit-order">
        <SubmitStatus status="submitted" />
        <h2>
          <FormattedMessage {..._messages.orderReference} /> <strong>
            {order.get('reference')}
          </strong>
        </h2>
        <section>
          <SubmitDeliveryDetails
            customerModel={order.get('customerModel')}
          />
          <SubmitOrderDetails
            paymentType={order.get('paymentType')}
            restaurantModel={order.get('restaurantModel')}
          />
        </section>
        <aside>
          <SubmitBasket
            items={order.get('items')}
            amount={order.get('amount')}
          />
        </aside>
      </section>
    );
  }

}

SubmitOrder = loading(SubmitOrder, ['order'], { isCollection: false });

SubmitOrder = fetch(orderActions.fetchOrderByReference)(SubmitOrder);

SubmitOrder = connect(state => ({
  order: state.order.order
}))(SubmitOrder);

export default SubmitOrder;
