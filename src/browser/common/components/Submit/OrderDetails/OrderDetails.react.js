import './OrderDetails.scss';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';
import Paper from '../../ui/Paper/Paper.react';
import PaymentType from '../../ui/Property/PaymentType.react';
import RestaurnatInfo from '../../ui/Property/RestaurantInfo.react';
import { defineMessages, FormattedMessage } from 'react-intl';
import Intl from '../../ui/Intl/Intl.react';
import AddressLine from '../../Address/Line/Line.react';
import { Link } from 'react-router';

const _messages = defineMessages({
  orderDetailsHeader: {
    defaultMessage: 'Order details',
    id: 'submit.order.details.header'
  }
});

class SubmitOrderDetails extends Component {
  static propTypes = {
    paymentType: PropTypes.string.isRequired,
    restaurantModel: PropTypes.object.isRequired
  };

  render() {
    const { paymentType, restaurantModel } = this.props;
    const header = <FormattedMessage {..._messages.orderDetailsHeader} />;

    return (
      <Paper header={header}>
        <section className="submit-delivery-from">
          <ul>
            <li>
              <PaymentType type={paymentType} />
            </li>
            <li>
              <RestaurnatInfo>
                <Link to={`restaurant/${restaurantModel.get('slug')}`}>
                  <Intl text={restaurantModel.get('name')} />
                </Link>
                <br />
                <AddressLine address={restaurantModel.get('addressModel')} />
              </RestaurnatInfo>
            </li>
          </ul>
        </section>
      </Paper>
    );
  }
}

export default SubmitOrderDetails;
