import './Basket.scss';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';
import { defineMessages, FormattedMessage } from 'react-intl';
import SubmitBasketItem from './Item.react';
import SubmitBasketSum from './Sum.react';

const _messages = defineMessages({
  yourOrder: {
    defaultMessage: 'Your Order',
    id: 'submit.basket.your.order'
  }
});

class SubmitBasket extends Component {

  static propTypes = {
    items: PropTypes.object.isRequired,
    amount: PropTypes.number.isRequired
  }

  render() {
    const { items, amount } = this.props;
    return (
      <section className="submit-basket">
        <header>
          <FormattedMessage {..._messages.yourOrder} />
        </header>
        <ul>
          {items.toSeq().map(item =>
            <SubmitBasketItem
              key={item.get('_id')}
              name={item.get('name')}
              price={item.get('price')}
              quantity={item.get('quantity')}
            />
          )}
        </ul>
        <footer>
          <SubmitBasketSum total={amount} />
        </footer>
      </section>
    );
  }

}

export default SubmitBasket;
