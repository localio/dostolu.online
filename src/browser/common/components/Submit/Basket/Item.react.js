import './Item.scss';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';
import Intl from '../../ui/Intl/Intl.react';
import Price from '../../ui/Price/Price.react';

class SubmitBasketItem extends Component {

  static propTypes = {
    quantity: PropTypes.number.isRequired,
    name: PropTypes.object.isRequired,
    price: PropTypes.number.isRequired
  }

  render() {
    const { quantity, name, price } = this.props;
    return (
      <li className="submit-basket-item">
        <span className="submit-basket__quantity">
          <span>{quantity}</span>
        </span>
        <Intl className="submit-basket-item__name" text={name} />
        <Price className="submit-basket-item__price price" price={price} />
      </li>
    );
  }

}

export default SubmitBasketItem;
