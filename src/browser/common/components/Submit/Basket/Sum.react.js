import './Sum.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import { defineMessages, FormattedMessage } from 'react-intl';
import Price from '../../ui/Price/Price.react';

const _messages = defineMessages({
  sum: {
    defaultMessage: 'Sum',
    id: 'submit.basket.total.sum'
  }
});

const SubmitBasketSum = (props) => {
  const { total } = props;

  return (
    <section className="submit-basket-sum">
      <dl>
        <dt>
          <FormattedMessage {..._messages.sum} />
        </dt>
        <dd>
          <Price className="submit-basket-sum__price" price={total} />
        </dd>
      </dl>
    </section>
  );
};

SubmitBasketSum.propTypes = {
  total: PropTypes.number
};

export default SubmitBasketSum;
