import './DeliveryDetails.scss';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';
import Paper from '../../ui/Paper/Paper.react';
import DeliveryDetails from '../../ui/Property/DeliveryDetails.react';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  deliveryDetailsHeader: {
    defaultMessage: 'Delivery details',
    id: 'submit.delivery.details.header'
  }
});

class SubmitDeliveryDetails extends Component {
  static propTypes = {
    customerModel: PropTypes.object.isRequired
  };

  render() {
    const { customerModel } = this.props;
    const locationModel = customerModel.get('locations').first();
    const instructions = locationModel.get('instructions');
    const header = <FormattedMessage {..._messages.deliveryDetailsHeader} />;

    return (
      <Paper header={header}>
        <section className="submit-details">
          <ul>
            <li>
              <DeliveryDetails
                name={customerModel.get('name')}
                apartment={locationModel.get('apartment')}
                address={locationModel.get('address')}
              />
            </li>
            {instructions && <li>{instructions}</li>}
          </ul>
        </section>
      </Paper>
    );
  }
}

export default SubmitDeliveryDetails;
