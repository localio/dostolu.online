import './Status.scss';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';
import classNames from 'classnames';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  statusSubmitted: {
    defaultMessage: 'Super! Your order has been placed!',
    id: 'submit.status.submitted'
  }
});

const resolveStatus = (status) => status ? 'submitted' : 'submitted';

class SubmitStatus extends Component {

  static propTypes = {
    status: PropTypes.string
  }

  render() {
    const { status } = this.props;
    const classes = classNames('submit-status', resolveStatus(status));
    return (
      <div className={classes}>
        <FormattedMessage {..._messages.statusSubmitted} />
      </div>
    );
  }
}

export default SubmitStatus;
