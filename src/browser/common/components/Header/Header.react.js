const logo = require('./logo.small.svg');
import './Header.scss';
import React from 'react';
import Link from 'react-router/lib/Link';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  headerMainTitle: {
    defaultMessage: 'dostolu',
    id: 'header.main.title'
  }
});

const Header = () =>
  <header id="header">
    <section>
      <Link className="headerLink" to="/">
        <img src={logo} role="presentation" />
        <span>
          <FormattedMessage {..._messages.headerMainTitle} />
        </span>
      </Link>
    </section>
  </header>;

export default Header;
