import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import connect from 'react-redux/lib/components/connect';

/**
 * Get intly string and get back current or default locale
 * @param props
 * @returns {XML}
 * @constructor
 */
const Intl = (props) => {
  const { text, className, currentLocale, defaultLocale } = props;
  const json = text.toJS();
  let string = '';
  if (currentLocale in json && json[currentLocale].length > 0) {
    string = json[currentLocale];
  } else if (defaultLocale in json) {
    string = json[defaultLocale];
  }

  return (
    <span className={className}>{string}</span>
  );
};

Intl.propTypes = {
  /**
   * Text to display.
   * Expecting INTL format where locale is a key
   * @var {Object}
   */
  text: PropTypes.object.isRequired,
  /**
   * Class Name
   */
  className: PropTypes.string,
  /**
   * Current locale
   */
  currentLocale: PropTypes.string,
  /**
   * Default locale
   */
  defaultLocale: PropTypes.string
};

export default connect(state => ({
  currentLocale: state.intl.currentLocale,
  defaultLocale: state.intl.defaultLocale
}))(Intl);
