import classNames from 'classnames';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';

/**
 * @param {Object} props
 * @returns {XML}
 * @constructor
 */
const Icon = (props) => {
  const icoCls = {};
  icoCls[`ico-${props.icon}`] = !!props.icon;
  icoCls[props.className] = !!props.className;
  const cls = classNames('ico', icoCls);
  return (
    <i className={cls} />
  );
};

Icon.propTypes = {
  /**
   * Icon name
   */
  icon: PropTypes.string.isRequired,
  /**
   * Class name for the icon
   */
  className: PropTypes.string
};

export default Icon;
