import './Footer.scss';
import React from 'react';
// import Link from 'react-router/lib/Link';

export default () => <footer id="footer">
  <div className="page">
    {/* <ul>
      <li>
        <Link to="/"><span>Home</span></Link>
      </li>
      <li>
        <Link to="/"><span>Home</span></Link>
      </li>
      <li>
        <Link to="/"><span>Home</span></Link>
      </li>
    </ul>*/ }
    <div className="footer-data">
      &copy; 2017 dostolu.online
    </div>
  </div>
</footer>;
