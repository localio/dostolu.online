import './Price.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import { FormattedNumber } from 'react-intl';
import classNames from 'classnames';

const Price = (props) => {
  const css = {};
  css.price = !props.className;
  css[props.className] = !!props.className;
  const className = classNames(css);
  return (
    <span className={className}>
      <FormattedNumber
        className={className}
        value={props.price}
        style="currency"
        currency="UAH"
        currencyDisplay="symbol"
      />
    </span>
  );
};

Price.propTypes = {
  price: PropTypes.number.isRequired,
  className: PropTypes.string
};

export default Price;
