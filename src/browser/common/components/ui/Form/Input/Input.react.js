import './Input.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Component from 'react/lib/ReactComponent';
import classNames from 'classnames';

class FormInput extends Component {

  static propTypes = {
    label: PropTypes.string.isRequired,
    type: PropTypes.string,
    name: PropTypes.string,
    error: PropTypes.string,
    value: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.onInputChange = this.onInputChange.bind(this);
    this.state = { active: false };
  }

  onInputChange(e) {
    const value = e.target.value;
    this.setState({
      active: value !== ''
    });
  }

  render() {
    const { label, name, error, ...others } = this.props;
    const inputType = this.props.type || 'text';
    let { active } = this.state;

    if (!!(this.props.value && this.props.value.length)) {
      active = true;
    }

    const spanCss = classNames({ active });
    const sectionCss = classNames('form-input', {
      error: !!error
    });
    const inputCss = classNames({ 'error-field': !!error });

    return (
      <section className={sectionCss}>
        <label>
          <span className={spanCss}>{label}</span>
          <input
            className={inputCss}
            type={inputType}
            placeholder={label}
            name={name}
            onChange={this.onInputChange}
            {...others}
          />
        </label>
        <i>{error}</i>
      </section>
    );
  }
}

export default FormInput;
