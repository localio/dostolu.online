import './Textarea.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Component from 'react/lib/ReactComponent';
import classNames from 'classnames';

class FormTextarea extends Component {

  static propTypes = {
    label: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    error: PropTypes.string,
    cols: PropTypes.number
  }

  constructor(props) {
    super(props);
    this.onInputChange = this.onInputChange.bind(this);
    this.state = { active: false };
  }

  onInputChange(e) {
    const value = e.target.value;
    this.setState({
      active: value !== ''
    });
  }

  render() {
    const { label, name, error, ...others } = this.props;
    const { active } = this.state;

    const spanCss = classNames({ active });
    const cols = this.props || 3;
    const sectionCss = classNames('form-textarea', {
      error: !!error
    });
    const textareaCss = classNames({ 'error-field': !!error });

    return (
      <section className={sectionCss}>
        <label>
          <span className={spanCss}>{label}</span>
          <textarea
            className={textareaCss}
            placeholder={label}
            name={name}
            cols={cols}
            onChange={this.onInputChange}
            {...others}
          ></textarea>
        </label>
        <i>{error}</i>
      </section>
    );
  }

}

export default FormTextarea;
