import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Component from 'react/lib/ReactComponent';
import connect from 'react-redux/lib/components/connect';
import * as uiActions from '../../../../../redux/ui/actions';

const TIMEOUT = 100;

class Page extends Component {

  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.object,
      PropTypes.func
    ]),
    clientRect: PropTypes.object,
    bottom: PropTypes.bool,
    setClientRect: PropTypes.func,
    setReachBottomFlag: PropTypes.func,
    setViewportHeight: PropTypes.func
  };

  static timeout;
  static lastTop = 0;

  constructor(props) {
    super(props);
    this.scrollTop = this.scrollTop.bind(this);
    this.processChanges = this.processChanges.bind(this);
  }

  componentDidMount() {
    if (window) {
      this.processChanges();
      window.addEventListener('scroll', this.processChanges);
      window.addEventListener('resize', this.processChanges);
      window.addEventListener('drag', this.processChanges);
    }
  }

  componentWillUnmount() {
    if (window) {
      window.removeEventListener('scroll');
      window.removeEventListener('resize');
      window.removeEventListener('drag');
    }
  }

  processChanges() {
    const { setClientRect, setReachBottomFlag, setViewportHeight, bottom } = this.props;
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      const rect = window.document.documentElement.getBoundingClientRect();
      setClientRect(rect);
      const docHeight = Math.max(
        document.body.scrollHeight,
        document.body.offsetHeight,
        document.documentElement.clientHeight,
        document.documentElement.scrollHeight,
        document.documentElement.offsetHeight
      );
      const position = rect.height - rect.top;
      const flag = position >= (docHeight - 140);
      if (flag !== bottom) {
        // If move slowly
        setReachBottomFlag(flag);
      } else if (flag && this.lastTop - rect.top > 0) {
        // If move very fast ro the next edge
        // Only in case of bottom reaching
        setReachBottomFlag(!flag);
        setReachBottomFlag(flag);
      }
      setViewportHeight(Math.max(document.documentElement.clientHeight, window.innerHeight || 0));
      this.lastTop = rect.top;
    }, TIMEOUT);
  }

  scrollTop() {
    if (window) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    const { children/* , clientRect */ } = this.props;
    // const jumpTop = clientRect && (clientRect.top < - clientRect.height / 2);

    return (
      <main>
        {children}
      </main>
    );
  }
}

export default connect(state => ({
  clientRect: state.ui.clientRect,
  bottom: state.ui.bottom
}), uiActions)(Page);
