import './Paper.scss';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';

class Paper extends Component {
  static propTypes = {
    children: PropTypes.object.isRequired,
    header: PropTypes.any
  }

  render() {
    const { children, header } = this.props;
    return (
      <section className="paper">
        {header &&
          <header>
            <h4>{header}</h4>
          </header>
        }
        <article>
          {children}
        </article>
      </section>
    );
  }
}

export default Paper;
