import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Property from './Property/Property.react';
import AddressFull from '../../Address/Full/Full.react';

const PropertyDeliveryDetails = (props) =>
  <Property icon="home">
    <AddressFull address={props.address} apartment={props.apartment} />
    {props.name}
  </Property>;

PropertyDeliveryDetails.propTypes = {
  address: PropTypes.object,
  name: PropTypes.string,
  apartment: PropTypes.string
};

export default PropertyDeliveryDetails;
