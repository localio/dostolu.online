import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Property from './Property/Property.react';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  restaurantIsClosed: {
    defaultMessage: 'Closed today',
    id: 'property.time.closed'
  }
});

const PropertyTime = (props) =>
  <Property icon="alarm-clock">
    <time>{props.time || <FormattedMessage {..._messages.restaurantIsClosed} />}</time>
  </Property>;

PropertyTime.propTypes = {
  time: PropTypes.string.isRequired
};

export default PropertyTime;
