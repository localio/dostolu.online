import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Property from './Property/Property.react';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  paymentType: {
    defaultMessage: 'Payment type',
    id: 'ui.property.payment'
  }
});

const PropertyPaymentType = (props) =>
  <Property icon="credit-card">
    <FormattedMessage {..._messages.paymentType} />:&nbsp;{props.type}
  </Property>;

PropertyPaymentType.propTypes = {
  type: PropTypes.any.isRequired
};

export default PropertyPaymentType;
