import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Property from './Property/Property.react';

const PropertyRestaurantInfo = (props) =>
  <Property icon="restaurant">
    {props.children}
  </Property>;

PropertyRestaurantInfo.propTypes = {
  children: PropTypes.any.isRequired
};

export default PropertyRestaurantInfo;
