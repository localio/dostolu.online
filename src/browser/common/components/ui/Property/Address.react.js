import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Property from './Property/Property.react';
import AddressLine from '../../Address/Line/Line.react';

const PropertyAddress = (props) =>
  <Property icon="placeholder">
    <AddressLine address={props.address} />
  </Property>;

PropertyAddress.propTypes = {
  address: PropTypes.object,
  apartment: PropTypes.string
};

export default PropertyAddress;
