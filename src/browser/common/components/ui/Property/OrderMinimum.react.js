import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Property from './Property/Property.react';
import Price from '../Price/Price.react';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  orderMinimum: {
    defaultMessage: 'Min.',
    id: 'ui.property.orderMinimum'
  }
});

const PropertyOrderMinimum = (props) =>
  <Property icon="shopping-cart">
    <FormattedMessage {..._messages.orderMinimum} />
    &nbsp;
    <Price price={props.minimum} className="property-order-minimum" />
  </Property>;

PropertyOrderMinimum.propTypes = {
  minimum: PropTypes.number.isRequired
};

export default PropertyOrderMinimum;
