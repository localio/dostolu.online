import './Property.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import PropertyIcon from '../Icon/Icon.react';

const Property = (props) =>
  <div className="ui-property">
    <PropertyIcon icon={props.icon} />
    <span className="ui-property-children">
      {props.children}
    </span>
  </div>;

Property.propTypes = {
  icon: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ])
};

export default Property;
