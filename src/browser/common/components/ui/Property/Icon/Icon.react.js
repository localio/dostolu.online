import './Icon.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Icon from '../../Icon/Icon.react';

const PropertyIcon = (props) => (
  <Icon icon={props.icon} className="ui-property-icon" />
);

PropertyIcon.propTypes = {
  /**
   * Icon name
   */
  icon: PropTypes.string.isRequired
};

export default PropertyIcon;
