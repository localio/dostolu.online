import './Loading.scss';
import React from 'react';

export default () =>
  <div className="loading">
    <div>
      <span className="dot dot_1" />
      <span className="dot dot_2" />
      <span className="dot dot_3" />
      <span className="dot dot_4" />
    </div>
  </div>;
