import './Button.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import { defineMessages, FormattedMessage } from 'react-intl';
import classNames from 'classnames';
import connect from 'react-redux/lib/components/connect';

const _messages = defineMessages({
  completeMyOrder: {
    defaultMessage: 'Place my order',
    id: 'checkout.button.complete'
  }
});

const CheckoutButton = (props) => {
  const { baskets, restaurant } = props;
  const restaurantId = restaurant.get('_id');
  // Get basket
  const basket = baskets.get(restaurantId);
  const isActive = basket && (restaurant.get('orderMinimum') <= basket.get('total'));
  const css = classNames({
    'action-button--inactive': !isActive,
    'action-button--active': isActive
  });

  return (
    <section className="checkout-button">
      <button className={css} type="submit" disabled={!isActive}>
        <FormattedMessage {..._messages.completeMyOrder} />
      </button>
    </section>
  );
};

CheckoutButton.propTypes = {
  baskets: PropTypes.object,
  restaurant: PropTypes.object
};

export default connect(state => ({
  baskets: state.basket.map,
  restaurant: state.restaurant.restaurant
}))(CheckoutButton);
