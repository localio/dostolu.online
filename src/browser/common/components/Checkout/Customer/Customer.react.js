import './Customer.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import FormInput from '../../ui/Form/Input/Input.react';
import FormTextarea from '../../ui/Form/Textarea/Textarea.react';
import { defineMessages, FormattedMessage } from 'react-intl';
import connect from 'react-redux/lib/components/connect';

const _messages = defineMessages({
  checkoutCustomerLabel: {
    defaultMessage: 'Contact information',
    id: 'checkout.customer.label'
  },
  checkoutCustomerName: {
    defaultMessage: 'Full name',
    id: 'checkout.customer.name'
  },
  checkoutCustomerEmail: {
    defaultMessage: 'Email address',
    id: 'checkout.customer.email'
  },
  checkoutCustomerPhone: {
    defaultMessage: 'Phone number',
    id: 'checkout.customer.phone'
  },
  checkoutDeliveryInstructions: {
    defaultMessage: 'Delivery instructions',
    id: 'checkout.delivery.instructions'
  },
  checkoutDeliveryAddress: {
    defaultMessage: 'Delivery address',
    id: 'checkout.delivery.address'
  },
  checkoutDeliveryApartment: {
    defaultMessage: 'Apartments number',
    id: 'checkout.delivery.apartment'
  }
});

const intlToText = (intl, currentLocale, defaultLocale) => intl.get(currentLocale || defaultLocale);

const CheckoutCustomer = (props) => {
  const { address, currentLocale, defaultLocale, fields } = props;
  let addressText = '';
  if (address) {
    addressText = `${intlToText(address.get('street').get('name'), currentLocale, defaultLocale)} `
      + `${address.get('place').get('housenumber')}`;
  }

  return (
    <fieldset className="checkout-customer">
      <legend>
        <FormattedMessage {..._messages.checkoutCustomerLabel} />
      </legend>
      <div>
        <FormattedMessage {..._messages.checkoutCustomerName}>
          {message => <FormInput
            label={message}
            type="text"
            required
            {...fields.name}
          />}
        </FormattedMessage>
        <FormattedMessage {..._messages.checkoutCustomerEmail}>
          {message => <FormInput
            label={message}
            type="email"
            required
            {...fields.email}
          />}
        </FormattedMessage>
        <FormattedMessage {..._messages.checkoutCustomerPhone}>
          {message => <FormInput
            label={message}
            type="tel"
            required
            {...fields.phone}
          />}
        </FormattedMessage>
        <FormattedMessage {..._messages.checkoutDeliveryAddress}>
          {message => <FormInput
            label={message}
            disabled="disabled"
            value={addressText}
          />}
        </FormattedMessage>
        <FormattedMessage {..._messages.checkoutDeliveryApartment}>
          {message => <FormInput
            label={message}
            type="text"
            required
            {...fields.apartment}
          />}
        </FormattedMessage>
        <FormattedMessage {..._messages.checkoutDeliveryInstructions}>
          {message => <FormTextarea
            label={message}
            {...fields.instructions}
          />}
        </FormattedMessage>
      </div>
    </fieldset>
  );
};

CheckoutCustomer.propTypes = {
  /**
   * Current address object
   */
  address: PropTypes.object,
  /**
   * Current locale
   */
  currentLocale: PropTypes.string,
  /**
   * Default locale
   */
  defaultLocale: PropTypes.string,
  /**
   * Fields object to handle fields
   */
  fields: PropTypes.object.isRequired
};

export default connect(state => ({
  address: state.address.currentAddress,
  currentLocale: state.intl.currentLocale,
  defaultLocale: state.intl.defaultLocale
}))(CheckoutCustomer);
