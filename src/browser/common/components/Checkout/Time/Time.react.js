import './Time.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Component from 'react/lib/ReactComponent';
import connect from 'react-redux/lib/components/connect';
import { defineMessages, FormattedMessage } from 'react-intl';

const DELIVERY_TIME_ASAP = 'asap';
// const DELIVERY_TIME_CHOOSE = 'choose';

const _messages = defineMessages({
  checkoutDeliveryTimeTarget: {
    defaultMessage: 'Delivery time',
    id: 'checkout.delivery.time.target'
  },
  checkoutDeliveryTimeASAP: {
    defaultMessage: 'ASAP',
    id: 'checkout.delivery.time.asap'
  },
  checkoutDeliveryTimeCustom: {
    defaultMessage: 'Choose time',
    id: 'checkout.delivery.time.custom'
  },
  checkoutDeliveryNoSlots: {
    defaultMessage: 'We are sorry, but this restaurant doesn\'t delivery any more '
      + 'today to your location. Please try to choose other restaurant.',
    id: 'checkout.delivery.no.slots'
  },
  checkoutDeliveryEstimatedTime: {
    defaultMessage: 'Estimated delivery time:',
    id: 'checkout.delivery.estimations'
  }
});

class CheckoutTime extends Component {

  static propTypes = {
    restaurant: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.onDeliveryTimeChange = this.onDeliveryTimeChange.bind(this);
    this.state = { chooseTime: false };
  }

  onDeliveryTimeChange(e) {
    const value = e.target.options[e.target.selectedIndex].value;
    this.setState({
      chooseTime: !(value === DELIVERY_TIME_ASAP)
    });
  }

  render() {
    const { restaurant } = this.props;
    const { chooseTime } = this.state;
    if (!restaurant) {
      return null;
    }
    const slots = restaurant.get('targetTimes');
    const estimations = (slots && slots.size > 0) ? slots.first() : null;

    return (
      <fieldset className="checkout-time">
        <FormattedMessage {..._messages.checkoutDeliveryTimeTarget} />
        <select defaultValue="asap" name="order[delivery]" onChange={this.onDeliveryTimeChange}>
          <FormattedMessage {..._messages.checkoutDeliveryTimeASAP}>
            {message => <option value={DELIVERY_TIME_ASAP}>{message}</option>}
          </FormattedMessage>
          {/* <FormattedMessage {..._messages.checkoutDeliveryTimeCustom}>
            {message => <option value={DELIVERY_TIME_CHOOSE}>{message}</option>}
          </FormattedMessage>*/}
        </select>
        {chooseTime &&
          <select>
            {restaurant.get('targetTimes').toSeq().map((slot, index) => {
              if (index === 0) return null;
              const range = `${slot.first()} - ${slot.last()}`;
              return (
                <option key={`delivery-slot-${index}`} value={range}>{range}</option>
              );
            })}
          </select>
        }
        {estimations
          ? <p>
            <FormattedMessage {..._messages.checkoutDeliveryEstimatedTime} />
            &nbsp;{estimations.join(' - ')}
          </p>
          : <p className="error"><FormattedMessage {..._messages.checkoutDeliveryNoSlots} /></p>
        }
      </fieldset>
    );
  }
}

export default connect(state => ({
  restaurant: state.restaurant.restaurant
}))(CheckoutTime);
