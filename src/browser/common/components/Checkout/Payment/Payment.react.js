import './Payment.scss';
import React from 'react';
// import PropTypes from 'react/lib/ReactPropTypes';
import Component from 'react/lib/ReactComponent';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  paymentOptions: {
    defaultMessage: 'Payment options:',
    id: 'checkout.payment.options'
  },
  paymentOptionCash: {
    defaultMessage: 'Cash',
    id: 'checkout.payment.option.cash'
  },
  paymentOptionCashNotes: {
    defaultMessage: 'Please pay with cash upon receiving your order.',
    id: 'checkout.payment.option.cash.notes'
  }
});

class CheckoutPayment extends Component {

  constructor(props) {
    super(props);
    this.onPaymentChange = this.onPaymentChange.bind(this);
  }

  onPaymentChange() {}

  render() {
    return (
      <fieldset className="checkout-payment">
        <FormattedMessage {..._messages.paymentOptions} />
        <select defaultValue="Cash" name="order[payment]" onChange={this.onPaymentChange}>
          <FormattedMessage {..._messages.paymentOptionCash}>
            {message => <option value="cash">{message}</option>}
          </FormattedMessage>
        </select>
        <div className="checkout-payment__type__cash">
          <p>
            <FormattedMessage {..._messages.paymentOptionCashNotes} />
          </p>
        </div>
      </fieldset>
    );
  }
}

export default CheckoutPayment;
