import './Checkout.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import CheckoutCustomer from './Customer/Customer.react';
import CheckoutTime from './Time/Time.react';
import CheckoutPayment from './Payment/Payment.react';
import CheckoutButton from './Button/Button.react';
import fields from '../../../../redux/lib/redux-fields/fields';
import connect from 'react-redux/lib/components/connect';
import * as customerActions from '../../../../redux/customer/actions';
import * as orderActions from '../../../../redux/order/actions';
import * as basketActions from '../../../../redux/basket/actions';
import * as locationActions from '../../../../redux/location/actions';
import { ORDER_TYPE_ASAP, ORDER_PAYMENT_TYPE_CASH } from '../../../../redux/order/Order';
import { push } from 'react-router-redux/lib/actions';

let Checkout = (props) => {
  const { fields } = props;
  const onSubmit = async (e) => {
    const {
      setCustomerName,
      setCustomerEmail,
      setCustomerPhone,
      setCustomerId,
      setLocationApartment,
      setLocationId,
      startOrder,
      setOrderType,
      setOrderPaymentType,
      setOrderReference,
      setLocationInstructions,
      setLocationPlace,
      submitOrder,
      finishOrder,
      cleanBasket,
      address,
      push
    } = props;
    e.preventDefault();
    const val = fields.$values();
    // console.log(val);
    // Start new order
    await startOrder();
    // Set location data
    await setLocationInstructions(val.instructions);
    await setLocationApartment(val.apartment);
    await setLocationPlace(address.get('currentId'));
    // Set order data
    await setOrderType(ORDER_TYPE_ASAP);
    await setOrderPaymentType(ORDER_PAYMENT_TYPE_CASH);
    // Set customer data
    await setCustomerName(val.name);
    await setCustomerEmail(val.email);
    await setCustomerPhone(val.phone);
    // Submit an order
    const payload = await submitOrder();
    if (payload && payload.data) {
      setCustomerId(payload.data.customer);
      setLocationId(payload.data.location);
      await setOrderReference({
        reference: payload.data.reference,
        uid: payload.data.uid
      });
      fields.$reset();
      await cleanBasket();
      await finishOrder();
      push(`/submit/${payload.data.reference}`);
    } else {
      // eslint-disable-next-line no-alert
      alert(
        'Seems like your order hasn\'t been submitted.\n' +
        'Something is wrong, please try again later'
      );
    }
  };

  return (
    <form className="checkout" onSubmit={onSubmit}>
      <CheckoutCustomer fields={fields} />
      <hr />
      <CheckoutTime fields={fields} />
      <hr />
      <CheckoutPayment />
      <hr />
      <CheckoutButton />
    </form>
  );
};

Checkout.propTypes = {
  address: PropTypes.object,
  fields: PropTypes.object,
  // Customer actions
  setCustomerName: PropTypes.func,
  setCustomerEmail: PropTypes.func,
  setCustomerPhone: PropTypes.func,
  setLocationApartment: PropTypes.func,
  setCustomerId: PropTypes.func,
  // Baset actions
  cleanBasket: PropTypes.func,
  // Location actions
  setLocationInstructions: PropTypes.func,
  setLocationPlace: PropTypes.func,
  setLocationId: PropTypes.func,
  // Order actions
  setOrderType: PropTypes.func,
  setOrderPaymentType: PropTypes.func,
  setOrderReference: PropTypes.func,
  startOrder: PropTypes.func,
  submitOrder: PropTypes.func,
  finishOrder: PropTypes.func,
  // Other
  push: PropTypes.func
};

Checkout = fields(Checkout, {
  path: '',
  fields: ['name', 'email', 'phone', 'apartment', 'instructions', 'deliveryType', 'deliveryTime']
});

export default connect(state => ({
  // restaurant: state.restaurant,
  address: state.address
}), { push, ...customerActions, ...orderActions, ...basketActions, ...locationActions })(Checkout);
