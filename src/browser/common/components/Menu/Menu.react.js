import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Component from 'react/lib/ReactComponent';
import connect from 'react-redux/lib/components/connect';
import * as menuUiActions from '../../../../redux/menuUi/actions';
import Loading from '../ui/Loading/Loading.react';
import MenuSectionsSelectorList from './SectionsSelector/List.react';
import MenuSectionsList from './Sections/List.react';

class Menu extends Component {
  static propTypes = {
    isRestaurantLoaded: PropTypes.bool,
    restaurant: PropTypes.object,
    menu: PropTypes.object,
    activeSection: PropTypes.string,
    setActiveSection: PropTypes.func,
    cleanActiveSection: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = { menuId: null };
  }

  async componentWillReceiveProps(props) {
    const {
      activeSection,
      cleanActiveSection,
      setActiveSection,
      isRestaurantLoaded,
      restaurant,
      menu
    } = props;
    const { menuId } = this.state;
    if (isRestaurantLoaded) {
      const newMenuId = restaurant.get('menu');
      if (newMenuId !== menuId) {
        this.setState({ menuId: newMenuId });
        cleanActiveSection();
      } else {
        if (!activeSection) {
          // Get very first section and set it up as a default
          if (menu) {
            const section = menu.get('sections').first();
            if (section) {
              setActiveSection(section.get('_id'));
            }
          }
        }
      }
    }
  }

  render() {
    const { menu, restaurant } = this.props;
    if (!menu) {
      return <Loading />;
    }
    return (
      <div>
        <nav>
          <MenuSectionsSelectorList sections={menu.get('sections')} />
        </nav>
        <section>
          <MenuSectionsList
            sections={menu.get('sections')}
            menuId={menu.get('_id')}
            restaurantId={restaurant.get('_id')}
            restaurantStatus={restaurant.get('status')}
          />
        </section>
      </div>
    );
  }
}

export default connect(state => ({
  isRestaurantLoaded: state.restaurant.loaded,
  restaurant: state.restaurant.restaurant,
  menu: state.menu.menu,
  activeSection: state.menuUi.activeSection
}), menuUiActions)(Menu);

