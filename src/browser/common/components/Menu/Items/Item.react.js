import './Item.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import Intl from '../../ui/Intl/Intl.react';
import Price from '../../ui/Price/Price.react';
import MenuIconAddToBasket from '../Icon/AddToBasket/AddToBasket.react';
import * as basketActions from '../../../../../redux/basket/actions';
import { isOnline } from '../../../../../redux/restaurant/lib/status';
import connect from 'react-redux/lib/components/connect';

const MenuItemsItem = (props) => {
  const { item, restaurantStatus } = props;
  const restaurantOnline = isOnline(restaurantStatus);

  const addItem = () => {
    if (!restaurantOnline) {
      return;
    }
    const { addItem, restaurantId, menuId } = props;
    addItem(item.get('_id'), item.get('price'), menuId, restaurantId);
  };

  return (
    <li className="menu-items-item">
      <div className="menu-items-item--description">
        <strong>
          <Intl text={item.get('name')} />
        </strong>
        <p>
          <Intl text={item.get('description')} />
        </p>
      </div>
      <div className="menu-items-item--price">
        {restaurantOnline
        ? <div className="menu-items-item--price--wrapper">
          <span onClick={addItem}>
            <Price price={item.get('price')} />
            <MenuIconAddToBasket className="menu-items-item--price--add" />
          </span>
        </div>
        : <span>
          <Price price={item.get('price')} />
        </span>
        }
      </div>
    </li>
  );
};

MenuItemsItem.propTypes = {
  item: PropTypes.object.isRequired,
  addItem: PropTypes.func,
  restaurantId: PropTypes.string.isRequired,
  restaurantStatus: PropTypes.number.isRequired,
  menuId: PropTypes.string.isRequired
};

export default connect(null, basketActions)(MenuItemsItem);
