import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import MenuItemsItem from './Item.react';

const MenuItemsList = (props) => {
  const { items, restaurantId, restaurantStatus, menuId } = props;
  return (
    <ul>
      {items.valueSeq().map(item =>
        <MenuItemsItem
          key={item.get('_id')}
          item={item}
          restaurantId={restaurantId}
          restaurantStatus={restaurantStatus}
          menuId={menuId}
        />
      )}
    </ul>
  );
};

MenuItemsList.propTypes = {
  items: PropTypes.object.isRequired,
  restaurantId: PropTypes.string.isRequired,
  restaurantStatus: PropTypes.number.isRequired,
  menuId: PropTypes.string.isRequired
};

export default MenuItemsList;
