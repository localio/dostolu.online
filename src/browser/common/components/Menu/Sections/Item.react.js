import './Item.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import connect from 'react-redux/lib/components/connect';
import classNames from 'classnames';
import Intl from '../../ui/Intl/Intl.react';
import MenuIconSection from '../Icon/Section/Section.react';
import MenuItemsList from '../Items/List.react';
import * as menuUiActions from '../../../../../redux/menuUi/actions';

const MenuSectionsItem = (props) => {
  const {
    section,
    activeSection,
    nonActiveSection,
    menuId,
    restaurantId,
    restaurantStatus
  } = props;
  const _id = section.get('_id');
  const opened = (_id === activeSection);
  const collapsed = (_id === activeSection && _id !== nonActiveSection);
  const cls = classNames('menu-sections-item', {
    'menu-sections-item--active': opened,
    'menu-section-items--collapsed': !collapsed
  });

  const onClick = () => {
    const { setActiveSection } = props;
    setActiveSection(_id);
  };

  return (
    <section className={cls}>
      <header onClick={onClick}>
        <MenuIconSection opened={collapsed} />
        <h4>
          <Intl text={section.get('name')} />
        </h4>
      </header>
      <div className="menu-sections-item-items">
        <MenuItemsList
          items={section.get('items')}
          restaurantId={restaurantId}
          restaurantStatus={restaurantStatus}
          menuId={menuId}
        />
      </div>
    </section>
  );
};

MenuSectionsItem.propTypes = {
  section: PropTypes.object.isRequired,
  activeSection: PropTypes.string,
  nonActiveSection: PropTypes.string,
  setActiveSection: PropTypes.func,
  restaurantId: PropTypes.string.isRequired,
  restaurantStatus: PropTypes.number.isRequired,
  menuId: PropTypes.string.isRequired
};

export default connect(state => ({
  activeSection: state.menuUi.activeSection,
  nonActiveSection: state.menuUi.nonActiveSection
}), menuUiActions)(MenuSectionsItem);
