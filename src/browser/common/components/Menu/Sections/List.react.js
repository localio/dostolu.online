import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import MenuSectionsItem from './Item.react';

const MenuSectionsList = (props) => {
  const { sections, restaurantId, restaurantStatus, menuId } = props;
  return (
    <div>
      {sections.valueSeq().map(section =>
        <MenuSectionsItem
          key={section.get('_id')}
          section={section}
          restaurantId={restaurantId}
          restaurantStatus={restaurantStatus}
          menuId={menuId}
        />
      )}
    </div>
  );
};

MenuSectionsList.propTypes = {
  sections: PropTypes.object.isRequired,
  restaurantId: PropTypes.string.isRequired,
  restaurantStatus: PropTypes.number.isRequired,
  menuId: PropTypes.string.isRequired
};

export default MenuSectionsList;
