import './Item.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import connect from 'react-redux/lib/components/connect';
import classNames from 'classnames';
import * as menuUiActions from '../../../../../redux/menuUi/actions';
import MenuIconSection from '../Icon/Section/Section.react';
import Intl from '../../ui/Intl/Intl.react';

const MenuSectionsSelectorItem = (props) => {
  const { section, activeSection } = props;
  const _id = section.get('_id');
  const opened = activeSection === _id;

  const onClick = () => {
    const { setActiveSection } = props;
    setActiveSection(_id);
  };

  const cls = classNames('menu-sections-selector-item', {
    'menu-sections-selector-item--active': opened
  });

  return (
    <li onClick={onClick} className={cls}>
      <MenuIconSection opened={opened} />
      <Intl text={section.get('name')} />
    </li>
  );
};

MenuSectionsSelectorItem.propTypes = {
  section: PropTypes.object.isRequired,
  activeSection: PropTypes.string,
  setActiveSection: PropTypes.func
};

export default connect(state => ({
  activeSection: state.menuUi.activeSection
}), menuUiActions)(MenuSectionsSelectorItem);
