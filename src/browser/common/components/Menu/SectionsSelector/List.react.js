import './List.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import MenuSectionsSelectorItem from './Item.react';

const MenuSectionsSelectorList = (props) => {
  const { sections } = props;
  return (
    <ul className="menu-sections-selector-list">
      {sections.valueSeq().map(section =>
        <MenuSectionsSelectorItem key={section.get('_id')} section={section} />
      )}
    </ul>
  );
};

MenuSectionsSelectorList.propTypes = {
  sections: PropTypes.object.isRequired
};

export default MenuSectionsSelectorList;
