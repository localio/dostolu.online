import React from 'react';
import Icon from '../../../ui/Icon/Icon.react';

const MenuIconAddToBasket = () =>
  <Icon icon="plus" />;

export default MenuIconAddToBasket;
