import './Section.scss';
import React from 'react';
import PropTypes from 'react/lib/ReactPropTypes';
import classNames from 'classnames';
import Icon from '../../../ui/Icon/Icon.react';

const MenuIconSection = (props) => {
  const cls = classNames(
    'menu-icon-section', {
      'menu-icon-section--opened': props.opened === true
    });
  return (
    <Icon icon="arrow-right" className={cls} />
  );
};

MenuIconSection.propTypes = {
  opened: PropTypes.bool
};

export default MenuIconSection;
