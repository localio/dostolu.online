// Bootstrap environment

const onWindowIntl = () => {
  require('babel-polyfill');
  window.Promise = require('../../redux/configureBluebird');

  // App locales are defined in src/server/config.js
  const { addLocaleData } = require('react-intl');
  const uk = require('react-intl/locale-data/uk');
  const de = require('react-intl/locale-data/de');
  const en = require('react-intl/locale-data/en');
  const pl = require('react-intl/locale-data/pl');
  const ru = require('react-intl/locale-data/ru');
  [uk, de, en, pl, ru].forEach(locale => addLocaleData(locale));

  require('./main');
};

// github.com/andyearnshaw/Intl.js/#intljs-and-browserifywebpack
if (!window.Intl) {
  require.ensure([
    'intl',
    'intl/locale-data/jsonp/uk.js',
    'intl/locale-data/jsonp/de.js',
    'intl/locale-data/jsonp/en.js',
    'intl/locale-data/jsonp/pl.js',
    'intl/locale-data/jsonp/ru.js'
  ], (require) => {
    require('intl');
    require('intl/locale-data/jsonp/uk.js');
    require('intl/locale-data/jsonp/de.js');
    require('intl/locale-data/jsonp/en.js');
    require('intl/locale-data/jsonp/pl.js');
    require('intl/locale-data/jsonp/ru.js');
    onWindowIntl();
  });
} else {
  onWindowIntl();
}
