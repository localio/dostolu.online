import App from '../app/App.react';
import HomePage from './pages/home/Home.react';
import RestaurantsPage from './pages/restaurants/Restaurants.react';
import RestaurantPage from './pages/restaurant/Restaurant.react';
import CheckoutPage from './pages/checkout/Checkout.react';
import SubmitPage from './pages/submit/Submit.react';
import NotFound from './pages/notFound/NotFound.react';
import React from 'react';
import IndexRoute from 'react-router/lib/IndexRoute';
import Route from 'react-router/lib/Route';

export default function createRoutes() {
// export default function createRoutes(getState) {
  // const requireViewer = (nextState, replace) => {
  //   if (getState().users.viewer) return;
  //   replace({
  //     pathname: '/signin',
  //     state: { nextPathname: nextState.location.pathname }
  //   });
  // };

  return (
    <Route component={App} path="/">
      <IndexRoute component={HomePage} />
      <Route component={RestaurantsPage} path="/restaurants/:town/:street" />
      <Route component={CheckoutPage} path="/restaurant/:slug/checkout" />
      <Route component={RestaurantPage} path="/restaurant/:slug" />
      <Route component={SubmitPage} path="/submit/:reference" />
      <Route component={NotFound} path="*" />
    </Route>
  );
}
// if (typeof require.ensure !== 'function') require.ensure = (d, c) => c(require)
//
// import App from '../app/App.react';
// // import HomePage from './pages/home/Home.react';
// //
// //
// // System.import('../../server/config');
// //
// // export default function createRoutes() {
// //   return {
// //     path: '/',
// //     component: App,
// //     getChildRoutes(location, cb) {
// //       require.ensure([], (require) => {
// //         cb(null, [
// //           require('./routes/Restaurants').default,
// //           require('./routes/Restaurant').default,
// //           require('./routes/NotFound').default
// //         ]);
// //       });
// //     },
// //     indexRoute: {
// //       component: HomePage
// //     }
// //   };
// // }
//
// const errorLoading = (err) => {
//   console.error('Dynamic page loading failed', err);
// };
//
// const loadRoute = (cb) => {
//   return (module) => cb(null, module.default);
// };
//
// export default function createRoutes() {
//   return {
//     component: App,
//     childRoutes: [
//       {
//         path: '/',
//         getComponenet(location, cb) {
//           System.import('./pages/home/Home.react')
//             .then(loadRoute(cb))
//             .catch(errorLoading);
//         }
//       }
//     ]
//   };
// }
