import React from 'react';
import ReactDOM from 'react-dom';
import configureStore from '../../redux/configureStore';
import createRoutes from './createRoutes';
import createStorageEngine from 'redux-storage-engine-localstorage';
import useScroll from 'react-router-scroll';
import Provider from 'react-redux/lib/components/Provider';
import Router from 'react-router/lib/Router';
import applyRouterMiddleware from 'react-router/lib/applyRouterMiddleware';
import browserHistory from 'react-router/lib/browserHistory';
import { fromJSON } from '../../redux/transit';
import routerMiddleware from 'react-router-redux/lib/middleware';
import syncHistoryWithStore from 'react-router-redux/lib/sync';

const initialState = fromJSON(window.__INITIAL_STATE__);
const store = configureStore({
  initialState,
  platformDeps: { createStorageEngine },
  platformMiddleware: [routerMiddleware(browserHistory)],
});
const history = syncHistoryWithStore(browserHistory, store);
const routes = createRoutes(store.getState);

ReactDOM.render(
  <Provider store={store}>
    <Router
      history={history}
      render={applyRouterMiddleware(useScroll())}
    >
      {routes}
    </Router>
  </Provider>
  , document.getElementById('app')
);
