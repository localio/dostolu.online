import NotFound from '../pages/notFound/NotFound.react';

export default {
  path: '*',
  component: NotFound
};
