import RestaurantsPage from '../pages/restaurants/Restaurants.react';

export default {
  path: '/restaurants/*',
  component: RestaurantsPage
};
