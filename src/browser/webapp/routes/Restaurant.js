import RestaurantPage from '../pages/restaurants/Restaurants.react';

export default {
  path: '/restaurant/:slug',
  component: RestaurantPage
};
