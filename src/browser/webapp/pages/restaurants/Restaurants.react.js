import './Restaurants.scss';
import React from 'react';
import Helmet from 'react-helmet';
import Header from '../../../common/components/Header/Header.react';
import Restaurants from '../../../common/components/Restaurants/Restaurants.react';

const RestaurantPage = () =>
  <div id="restaurants-page">
    <Helmet title="Dostolu.online" />
    <Header />
    <div className="page">
      <Restaurants />
    </div>
  </div>;

export default RestaurantPage;
