import './Restaurant.scss';
import React from 'react';
import Helmet from 'react-helmet';
import Header from '../../../common/components/Header/Header.react';
import Restaurant from '../../../common/components/Restaurant/Restaurant.react';
import Menu from '../../../common/components/Menu/Menu.react';
import BasketSticky from '../../../common/components/Basket/Sticky/Sticky.react';
import Basket from '../../../common/components/Basket/Basket.react';
import fetch from '../../../../common/lib/fetch/fetch';
import * as restaurantActions from '../../../../redux/restaurant/actions';
import * as menuActions from '../../../../redux/menu/actions';

let RestaurantPage = (props) =>
  <div id="restaurant-page">
    <Helmet title="Dostolu.online" />
    <Header />
    <div className="page">
      <aside>
        <Restaurant {...props} />
        <Basket checkoutButton />
      </aside>
      <section>
        <Menu {...props} />
      </section>
    </div>
    <div className="sticky-basket">
      <BasketSticky />
    </div>
  </div>;

RestaurantPage = fetch([
  restaurantActions.fetchRestaurantBySlug,
  menuActions.fetchMenuById
])(RestaurantPage);

export default RestaurantPage;
