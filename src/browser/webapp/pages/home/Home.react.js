import './Home.scss';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import Helmet from 'react-helmet';
import Header from '../../../common/components/Header/Header.react';
import AddressWelcome from '../../../common/components/Address/Welcome/Welcome.react';
import Cards from '../../../common/components/Cards/Cards.react';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  cardTitle1: {
    defaultMessage: 'header 1',
    id: 'home.card.title.1'
  },
  cardText1: {
    defaultMessage: 'text 1',
    id: 'home.card.text.1'
  },
  cardTitle2: {
    defaultMessage: 'header 2',
    id: 'home.card.title.2'
  },
  cardText2: {
    defaultMessage: 'text 2',
    id: 'home.card.text.2'
  }
});

class HomePage extends Component {

  render() {
    return (
      <div id="home-page">
        <Helmet title="Dostolu.online" />
        <Header />
        <AddressWelcome />
        <Cards />
        <div className="home-page-text page">
          <section>
            <h4>
              <FormattedMessage {..._messages.cardTitle1} />
            </h4>
            <p>
              <FormattedMessage {..._messages.cardText1} />
            </p>
          </section>
          <section>
            <h4>
              <FormattedMessage {..._messages.cardTitle2} />
            </h4>
            <p>
              <FormattedMessage {..._messages.cardText2} />
            </p>
          </section>
        </div>
      </div>
    );
  }
}

export default HomePage;
