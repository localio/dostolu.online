import './NotFound.scss';
import Helmet from 'react-helmet';
import Header from '../../../common/components/Header/Header.react';
import React from 'react';
import Link from 'react-router/lib/Link';
import { defineMessages, FormattedMessage } from 'react-intl';

const _messages = defineMessages({
  notFoundBack: {
    defaultMessage: 'Bring me back',
    id: 'not.found.back'
  },
  notFoundMessage: {
    defaultMessage: 'It feels like this page has been eaten.\nPlease try again!',
    id: 'not.found.message'
  }
});

const NotFoundPage = () =>
  <div id="not-found-page">
    <Helmet title="Dostolu.online" />
    <Header />
    <div className="page">
      <section>
        <img role="presentation" src={require('./not-found.png')} />
        <p>
          <FormattedMessage {..._messages.notFoundMessage} />
        </p>
        <Link to="/" className="action-button--active">
          <span>
            <FormattedMessage {..._messages.notFoundBack} />
          </span>
        </Link>
      </section>
    </div>
  </div>;

export default NotFoundPage;
