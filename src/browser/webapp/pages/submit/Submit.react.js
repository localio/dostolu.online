import './Submit.scss';
import React from 'react';
import Component from 'react/lib/ReactComponent';
import PropTypes from 'react/lib/ReactPropTypes';
import Helmet from 'react-helmet';
import Header from '../../../common/components/Header/Header.react';
import SubmitOrder from '../../../common/components/Submit/Order/Order.react';
import connect from 'react-redux/lib/components/connect';

class SubmitPage extends Component {
  static propTypes = {
    storageLoaded: PropTypes.bool
  };

  render() {
    const { storageLoaded } = this.props;
    return (
      <div id="submit-page">
        <Helmet title="Dostolu.online" />
        <Header />
        <div className="page">
          {storageLoaded && <SubmitOrder {...this.props} />}
        </div>
      </div>
    );
  }
}

SubmitPage = connect(state => ({
  storageLoaded: state.app.storageLoaded
}))(SubmitPage);

export default SubmitPage;
