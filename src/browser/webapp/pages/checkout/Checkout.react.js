import './Checkout.scss';
import React from 'react';
import Helmet from 'react-helmet';
import Header from '../../../common/components/Header/Header.react';
import Restaurant from '../../../common/components/Restaurant/Restaurant.react';
import Basket from '../../../common/components/Basket/Basket.react';
import Checkout from '../../../common/components/Checkout/Checkout.react';
import fetch from '../../../../common/lib/fetch/fetch';
import * as restaurantActions from '../../../../redux/restaurant/actions';
import * as menuActions from '../../../../redux/menu/actions';


let CheckoutPage = (props) =>
  <div id="checkout-page">
    <Helmet title="Dostolu.online" />
    <Header />
    <div className="page">
      <aside>
        <Restaurant {...props} />
        <Basket checkoutButton={false} />
      </aside>
      <section>
        <Checkout {...props} />
      </section>
    </div>
  </div>;

CheckoutPage = fetch([
  restaurantActions.fetchRestaurantBySlug,
  menuActions.fetchMenuById
])(CheckoutPage);

export default CheckoutPage;
