const winston = require('winston');
const config = require('../config');

winston.cli();

module.exports = (module) => {
  // Include filename
  const path = module.filename.split('/').slice(-2).join('/');
  return new winston.Logger({
    transports: [
      // Use console
      new winston.transports.Console({
        colorize: false,
        level: config.isProduction ? 'warning' : 'debug',
        label: path
      })
    ]
  });
};
