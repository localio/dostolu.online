import config from './config';
import errorHandler from './lib/errorHandler';
import express from 'express';
import frontend from './frontend';
import https from 'https';
import http from 'http';
import morgan from 'morgan';
import fs from 'fs';

const runWebserv = (app, params) => params
  ? https.createServer(params, app)
  : http.createServer(app);

const { PORT, SSL_CERT_PATH, SSL_KEY_PATH } = process.env;

const app = express();

app.get('/status', (req, res) => {
  res.status(200).send('');
});

// Enable access log
app.use(morgan('combined'));

app.get('*', (req, res, next) => {
  if (req.headers['x-forwarded-proto'] && req.headers['x-forwarded-proto'] === 'http') {
    res.redirect(`https://${req.headers.host}${req.url}`);
  } else {
    next();
  }
});

app.use(frontend);
app.use(errorHandler);

runWebserv(app, SSL_KEY_PATH && SSL_CERT_PATH && {
  key: fs.readFileSync(SSL_KEY_PATH, 'utf-8'),
  cert: fs.readFileSync(SSL_CERT_PATH, 'utf-8')
}).listen(PORT || config.port, () => {
  console.log(`Server started at http://localhost:${config.port}`);
});
