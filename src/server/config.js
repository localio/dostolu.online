// www.andrewsouthpaw.com/2015/02/08/environment-variables/
import nconf from 'nconf';

// Use less-terrible separator character, stackoverflow.com/questions/25017495
nconf.env('__');

let firebaseConfig;
let apiConfig;
try {
  /* eslint import/no-unresolved: 0 */
  const secrets = require('./_secrets.json');
  firebaseConfig = secrets.firebase;
  apiConfig = secrets.api;
} catch (e) {
  firebaseConfig = {
    apiKey: process.env.FIREBASE_API_KEY,
    authDomain: process.env.FIREBASE_AUTH_DOMAIN,
    databaseURL: process.env.FIREBASE_DATA_URL,
    storageBucket: process.env.FIREBASE_STORAGE_BUCKET,
    messagingSenderId: process.env.FIREBASE_MESSAGING_SENDER_ID
  };
  apiConfig = process.env.API_PATH ? {
    path: process.env.API_PATH
  } : null;
}

// Remember, never put secrets in the source code. Use environment variables for
// production or src/common/config.json for development instead.
nconf.defaults({
  appName: require('../../package.json').name,
  // Use appVersion defined in gulp env task or Heroku dyno metadata.
  appVersion: process.env.appVersion,
  defaultLocale: 'uk',
  googleAnalyticsId: 'UA-92191101-1',
  isProduction: process.env.NODE_ENV === 'production',
  locales: ['uk', 'de', 'en', 'pl', 'ru'],
  port: process.env.PORT || 8000,
  firebase: firebaseConfig,
  // Enable hot reload on remote device. Note it prevents offline testing,
  // because it depends on ip.address(), which doesn't work with disabled wifi.
  // How do we access a website running on localhost from mobile browser?
  // stackoverflow.com/questions/3132105
  remoteHotReload: false,
  api: apiConfig || {
    path: 'http://0.0.0.0:5001'
  }
});

// For local development, we can override defaults easily. Rename
// src/common/_config.json to src/common/config.json and uncomment next line.

export default nconf.get();
