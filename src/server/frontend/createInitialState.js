import config from '../config';
import configReducer from '../../redux/config/reducer';
import deviceReducer from '../../redux/device/reducer';
import intlReducer from '../../redux/intl/reducer';
import loadMessages from '../intl/loadMessages';

const messages = loadMessages();

export default function createInitialState() {
  return {
    config: configReducer(undefined, {})
      .set('appName', config.appName)
      .set('appVersion', config.appVersion)
      .set('firebase', config.firebase)
      .set('api', config.api),
    device: deviceReducer(undefined, {}),
    intl: intlReducer(undefined, {})
      .set('currentLocale', config.defaultLocale)
      .set('defaultLocale', config.defaultLocale)
      .set('locales', config.locales)
      .set('messages', messages)
  };
}
