import Helmet from 'react-helmet';
import Html from './Html.react';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import config from '../config';
import configureStore from '../../redux/configureStore';
import createInitialState from './createInitialState';
import createWebAppRoutes from '../../browser/webapp/createRoutes';
import serialize from 'serialize-javascript';
import { Provider } from 'react-redux';
import { createMemoryHistory, match, RouterContext } from 'react-router';
// import { queryFirebaseServer } from '../../redux/lib/redux-firebase/queryFirebase';
import { routerMiddleware, syncHistoryWithStore } from 'react-router-redux';
import { toJSON } from '../../redux/transit';
const log = require('../lib/log')(module);
// import { TOKEN_NAME } from '../../redux/lib/auth/token';

const initialState = createInitialState();

const fetchComponentDataAsync = async (dispatch, renderProps) => {
  const { components, location, params } = renderProps;
  const promises = components
    .reduce((actions, component) => {
      if (typeof component === 'function') {
        actions = actions.concat(component.fetchActions || []);
      } else {
        Object.keys(component).forEach(c => {
          actions = actions.concat(component[c].fetchActions || []);
        });
      }
      return actions;
    }, [])
    .map(action => {
      if (action instanceof Array) {
        return action.reduce(
            (prev, curr) => prev.then(() => dispatch(curr({ location, params }))),
            Promise.resolve()
          );
      }
      return dispatch(action({ location, params }));
    });
  await Promise.all(promises);
};

const createRequestInitialState = req => {
  const currentLocale = config.defaultLocale;
  // const currentLocale = process.env.IS_SERVERLESS
  //   ? config.defaultLocale
  //   : req.acceptsLanguages(config.locales) || config.defaultLocale;
  const host =
    `${req.headers['x-forwarded-proto'] || req.protocol}://${req.headers.host}`;
  return {
    ...initialState,
    intl: initialState.intl
      .set('currentLocale', currentLocale)
      .set('initialNow', Date.now()),
    device: initialState.device
      .set('host', host),
  };
};

const renderApp = (store, renderProps) => {
  const appHtml = ReactDOMServer.renderToString(
    <Provider store={store}>
      <RouterContext {...renderProps} />
    </Provider>
  );
  return { appHtml, helmet: Helmet.rewind() };
};

const renderScripts = (state, headers, hostname, appJsFilename) =>
  // https://github.com/yahoo/serialize-javascript#user-content-automatic-escaping-of-html-characters
  // TODO: Switch to CSP, https://github.com/este/este/pull/731
  `
    <script>
      window.__INITIAL_STATE__ = ${serialize(toJSON(state))};
    </script>
    <script src="${appJsFilename}"></script>
  `;

const renderPage = (store, renderProps, req, scope) => {
  const state = store.getState();
  // No server routing for server-less apps.
  if (process.env.IS_SERVERLESS) {
    delete state.routing;
  }
  const { headers, hostname } = req;
  const { appHtml, helmet } = renderApp(store, renderProps);

  const assets = webpackIsomorphicTools.assets();
  const appCssFilename = assets.styles[scope];
  const appJsFilename = assets.javascript[scope];

  const scriptsHtml = renderScripts(state, headers, hostname, appJsFilename);
  if (!config.isProduction) {
    webpackIsomorphicTools.refresh();
  }
  const docHtml = ReactDOMServer.renderToStaticMarkup(
    <Html
      appCssFilename={appCssFilename}
      bodyHtml={`<div id="app">${appHtml}</div>${scriptsHtml}`}
      googleAnalyticsId={config.googleAnalyticsId}
      helmet={helmet}
      isProduction={config.isProduction}
      scope={scope}
    />
  );
  return `<!DOCTYPE html>${docHtml}`;
};

export default async function render(req, res, next) {
  const memoryHistory = createMemoryHistory(req.originalUrl);
  const store = configureStore({
    initialState: createRequestInitialState(req),
    platformMiddleware: [routerMiddleware(memoryHistory)],
    logger: log
  });
  const history = syncHistoryWithStore(memoryHistory, store);

  /**
   * Use different app parts if customer isn't logged in
   * @todo: Check JWT right here right now? So It won't return any content
   * As it is possible just a cookie and get javascript compiled for the
   */
  let scope = null;
  let routes = null;


  routes = createWebAppRoutes(store.getState);
  scope = 'app';

  const location = req.url;

  match({ history, routes, location }, async (error, redirectLocation, renderProps) => {
    if (redirectLocation) {
      res.redirect(301, redirectLocation.pathname + redirectLocation.search);
      return;
    }
    if (error) {
      next(error);
      return;
    }
    try {
      if (!process.env.IS_SERVERLESS) {
        // @todo: I need both, firebase and fetch and I don't like how it is done
        await fetchComponentDataAsync(store.dispatch, renderProps);
        // await queryFirebaseServer(() => renderApp(store, renderProps));
      }
      const html = renderPage(store, renderProps, req, scope);
      const status = renderProps.routes
        .some(route => route.path === '*') ? 404 : 200;
      res.status(status).send(html);
    } catch (error) {
      next(error);
    }
  });
}
