import * as actions from './actions';
import Customer from './Customer';
import { Record } from '../transit';

const InitialState = Record({
  customer: null
}, 'customer');

const initState = state =>
  state.get('customer') === null
    ? state.set('customer', new Customer())
    : state;

export default function customerReducer(state = new InitialState, action) {
  switch (action.type) {
    case actions.SET_CUSTOMER_NAME: {
      return initState(state)
        .setIn(['customer', 'name'], action.payload);
    }
    case actions.SET_CUSTOMER_EMAIL: {
      return initState(state)
        .setIn(['customer', 'email'], action.payload);
    }
    case actions.SET_CUSTOMER_PHONE: {
      return initState(state)
        .setIn(['customer', 'phone'], action.payload);
    }
    case actions.SET_CUSTOMER_ID: {
      return initState(state)
        .setIn(['customer', '_id'], action.payload);
    }
  }
  return state;
}
