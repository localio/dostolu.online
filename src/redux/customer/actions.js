export const SET_CUSTOMER_NAME = 'SET_CUSTOMER_NAME';
export const SET_CUSTOMER_EMAIL = 'SET_CUSTOMER_EMAIL';
export const SET_CUSTOMER_PHONE = 'SET_CUSTOMER_PHONE';
export const SET_CUSTOMER_ID = 'SET_CUSTOMER_ID';

export function setCustomerName(payload) {
  return {
    type: 'SET_CUSTOMER_NAME',
    payload
  };
}

export function setCustomerEmail(payload) {
  return {
    type: 'SET_CUSTOMER_EMAIL',
    payload
  };
}

export function setCustomerPhone(payload) {
  return {
    type: 'SET_CUSTOMER_PHONE',
    payload
  };
}

export function setCustomerId(payload) {
  return {
    type: 'SET_CUSTOMER_ID',
    payload
  };
}
