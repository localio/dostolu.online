import Immutable from 'immutable';
import Location from '../location/Location';

const Customer = class {
  constructor(data = null) {
    const locations = [];
    if (data && data.locations && data.locations.length) {
      data.locations.forEach(location => locations.push(new Location(location)));
    }
    return Immutable.fromJS(data ? {
      _id: data._id,
      name: data.name,
      email: data.email,
      phone: data.phone,
      locations: Immutable.fromJS(locations)
    } : {});
  }
};

export default Customer;
