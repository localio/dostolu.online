export const PREFILL_RESTAURANT = 'PREFILL_RESTAURANT';

export const FETCH_RESTAURANT_ERROR = 'FETCH_RESTAURANT_ERROR';
export const FETCH_RESTAURANT_START = 'FETCH_RESTAURANT_START';
export const FETCH_RESTAURANT_SUCCESS = 'FETCH_RESTAURANT_SUCCESS';

export function prefillRestaurant(restaurant) {
  return {
    type: 'PREFILL_RESTAURANT',
    payload: restaurant.toJS()
  };
}

export function fetchRestaurantBySlug(payload) {
  const { params: { slug } } = payload;
  return ({ fetch, getState }) => {
    const promise = fetch(
      `${getState().config.api.path}/restaurants?filters[slug]=${slug}`, {
        method: 'GET',
        headers: {
          accept: 'application/json'
        },
        mode: 'cors'
      })
      .then(json => ({ data: json.data }));
    return {
      type: 'FETCH_RESTAURANT',
      payload: promise
    };
  };
}

export function fetchRestaurantById(id) {
  return ({ fetch, getState }) => {
    const promise = fetch(
      `${getState().config.api.path}/restaurants/${id}`, {
        method: 'GET',
        headers: {
          accept: 'application/json'
        },
        mode: 'cors'
      })
      .then(json => ({
        data: json.data
      }));
    return {
      type: 'FETCH_RESTAURANT',
      payload: promise
    };
  };
}
