import * as actions from './actions';
import Restaurant from './Restaurant';
import { Record } from '../transit';
// import { Map } from 'immutable';
// import { collectionToOrderedSet } from '../lib/reducers';

const InitialState = Record({
  restaurant: null,
  /**
   * False if restaurant is not loaded, but we still could have
   * small amount of data from the other system part
   */
  loaded: false
}, 'restaurant');

export default function restaurantReducer(state = new InitialState, action) {
  switch (action.type) {
    case actions.PREFILL_RESTAURANT: {
      const data = action.payload;
      return state.set('restaurant', new Restaurant(data)).set('loaded', false);
    }
    case actions.FETCH_RESTAURANT_SUCCESS: {
      const { data } = action.payload;
      return state.set('restaurant', new Restaurant(data)).set('loaded', true);
    }
  }
  return state;
}
