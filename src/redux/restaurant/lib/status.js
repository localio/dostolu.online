import { restaurants } from '@dostolu/constants';

const statuses = restaurants.status;

/**
 * Check whenever restaurant is online
 * @param {Number} status
 */
const isOnline = status =>
  parseInt(Math.floor(status / 1000), 10)
    === parseInt(statuses.RESTAURANT_STATUS_OPEN / 1000, 10);

/**
 * Checn whenever restaurant is offline
 * @param {Number} status
 */
const isOffline = status =>
  parseInt(Math.floor(status / 1000), 10)
    === parseInt(statuses.RESTAURANT_STATUS_CLOSED / 1000, 10);

export {
  isOnline,
  isOffline
};
