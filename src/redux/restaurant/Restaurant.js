import Immutable from 'immutable';
import Cuisine from '../cuisine/Cuisine';
import Address from '../address/Address';

const Restaurant = class {
  constructor(data) {
    return Immutable.fromJS({
      _id: data._id,
      name: data.name,
      slug: data.slug,
      orderMinimum: data.orderMinimum,
      menu: data.menu,
      geometry: data.geometry,
      type: data.type,
      description: data.description,
      openingHours: data.openingHours,
      openedToday: data.openedToday,
      targetTimes: data.targetTimes,
      status: data.status,
      logo: data.logo,
      cuisineModel: data.cuisineModel ? new Cuisine(data.cuisineModel) : null,
      addressModel: data.addressModel ? new Address(data.addressModel) : null
    });
  }
};

export default Restaurant;
