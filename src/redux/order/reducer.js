import * as actions from './actions';
import Order from './Order';
import { Record } from '../transit';
import { Map } from 'immutable';

const InitialState = Record({
  order: null,
  orders: null,
  references: Map()
}, 'order');

export default function orderReducer(state = new InitialState, action) {
  switch (action.type) {
    case actions.START_ORDER: {
      const { restaurantId, menuId } = action.payload;
      state = state.set('order', new Order());
      return state
        .setIn(['order', 'restaurant'], restaurantId)
        .setIn(['order', 'menu'], menuId);
    }
    case actions.FINISH_ORDER: {
      return state.set('order', null);
    }
    case actions.SET_ORDER_TYPE: {
      return state.setIn(['order', 'type'], action.payload);
    }
    case actions.SET_ORDER_PAYMENT_TYPE: {
      return state.setIn(['order', 'paymentType'], action.payload);
    }
    case actions.SET_ORDER_REFERENCE: {
      const { reference, uid } = action.payload;
      return state.setIn(['references', reference], uid);
    }
    case actions.REMOVE_ORDER_REFERENCE: {
      return state.removeIn(['references', action.payload]);
    }
    case actions.CLEAN_ORDER_RENCECES: {
      return state.set('references', []);
    }
    case actions.FETCH_ORDER_SUCCESS: {
      const { data } = action.payload;
      if (data) {
        return state.set('order', new Order(data));
      }
      return state;
    }
  }
  return state;
}
