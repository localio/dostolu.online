import OrderRequest from './Request';

export const SET_ORDER_TYPE = 'SET_ORDER_TYPE';
export const SET_ORDER_PAYMENT_TYPE = 'SET_ORDER_PAYMENT_TYPE';
export const START_ORDER = 'START_ORDER';

export const SUBMIT_ORDER_ERROR = 'SUBMIT_ORDER_ERROR';
export const SUBMIT_ORDER_START = 'SUBMIT_ORDER_START';
export const SUBMIT_ORDER_SUCCESS = 'SUBMIT_ORDER_SUCCESS';

export const SET_ORDER_REFERENCE = 'SET_ORDER_REFERENCE';
export const REMOVE_ORDER_REFERENCE = 'REMOVE_ORDER_REFERENCE';
export const CLEAN_ORDER_RENCECES = 'CLEAN_ORDER_RENCECES';

export const FETCH_ORDER_ERROR = 'FETCH_ORDER_ERROR';
export const FETCH_ORDER_START = 'FETCH_ORDER_START';
export const FETCH_ORDER_SUCCESS = 'FETCH_ORDER_SUCCESS';

export const FINISH_ORDER = 'FINISH_ORDER';

export function fetchOrderByReference(payload) {
  const { params: { reference } } = payload;
  return ({ fetch, getState }) => {
    const uid = getState().order.getIn(['references', reference]);
    let promise = null;
    if (uid) {
      promise = fetch(
        `${getState().config.api.path}/orders/${reference}?uid=${uid}`, {
          method: 'GET',
          headers: {
            accept: 'application/json'
          },
          mode: 'cors'
        })
        .then(json => ({ data: json.data }));
    } else {
      promise = Promise.resolve({
        data: null
      });
    }
    return {
      type: 'FETCH_ORDER',
      payload: promise
    };
  };
}

export function startOrder() {
  return ({ getState }) => {
    const restaurant = getState().restaurant.restaurant;
    const restaurantId = restaurant.get('_id');
    const menu = getState().menu.menu;
    const menuId = menu.get('_id');
    return {
      type: 'START_ORDER',
      payload: { restaurantId, menuId }
    };
  };
}

export function setOrderType(type) {
  return {
    type: 'SET_ORDER_TYPE',
    payload: type
  };
}

export function setOrderPaymentType(paymentType) {
  return {
    type: 'SET_ORDER_PAYMENT_TYPE',
    payload: paymentType
  };
}

export function submitOrder() {
  return ({ fetch, getState/* , dispatch*/ }) => {
    const request = new OrderRequest(getState());
    // console.log(JSON.stringify(request));
    // console.log(dispatch);
    const promise = fetch(
      `${getState().config.api.path}/orders`, {
        method: 'post',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json'
        },
        mode: 'cors',
        body: JSON.stringify(request)
      });
    return {
      type: 'SUBMIT_ORDER',
      payload: promise
    };
  };
}

export function finishOrder() {
  return {
    type: 'FINISH_ORDER',
    payload: null
  };
}

export function setOrderReference(payload) {
  const { reference, uid } = payload;
  return {
    type: 'SET_ORDER_REFERENCE',
    payload: { reference, uid }
  };
}

export function removeOrderReference(payload) {
  return {
    type: 'REMOVE_ORDER_REFERENCE',
    payload
  };
}

export function cleanOrderReferences() {
  return {
    type: 'CLEAN_ORDER_REFERENCES'
  };
}

