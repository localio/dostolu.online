class OrderRequest {

  orderType = null;
  orderPaymentType = null;
  restaurant = null;
  menu = null;
  basket = null;
  customer = null;
  location = null;

  constructor(state = null) {
    if (state !== null) {
      this.setOrder(state.order.get('order'));
      this.setBasket(state.basket.get('map'));
      this.setLocation(state.location.get('location'));
      this.setCustomer(state.customer.get('customer'));
    }
  }

  setOrder(order) {
    this.orderType = order.get('type');
    this.orderPaymentType = order.get('paymentType');
    this.menu = order.get('menu');
    this.restaurant = order.get('restaurant');
  }

  setBasket(basket) {
    this.basket = basket.get(this.restaurant).toJS();
  }

  setCustomer(customer) {
    this.customer = customer.toJS();
  }

  setLocation(location) {
    this.location = location.toJS();
  }

}

export default OrderRequest;
