import Immutable from 'immutable';
import Restaurant from '../restaurant/Restaurant';
import Customer from '../customer/Customer';

export const ORDER_TYPE_ASAP = 'asap';
export const ORDER_PAYMENT_TYPE_CASH = 'cash';

const Order = class {
  constructor(data = null) {
    return Immutable.fromJS(data ? {
      amount: data.amount || 0,
      deliveryType: data.deliveryType,
      items: Immutable.fromJS(data.items),
      paymentType: data.paymentType,
      reference: data.reference,
      status: data.status,
      customerModel: new Customer(data.customerModel),
      restaurantModel: new Restaurant(data.restaurantShortModel)
    } : { });
  }
};

export default Order;
