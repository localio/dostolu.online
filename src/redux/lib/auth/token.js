import Cookies from 'js-cookie';

export const TOKEN_NAME = 'toktok';

/**
 * Get token from the current storage
 */
const getToken = () => Cookies.get(TOKEN_NAME);

/**
 * Store token in a current storage
 * @param token
 */
export const storeToken = token => !!token && Cookies.set(TOKEN_NAME, token);

/**
 * Remove token from the cusrrent storage
 */
export const removeToken = () => Cookies.remove(TOKEN_NAME);

/**
 * Add token to the headers list
 * @param headers
 * @return {*}
 */
export const tokenize = headers => {
  headers.authorization = `Bearer ${getToken()}`;
  return headers;
};
