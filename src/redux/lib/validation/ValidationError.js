import { BaseError } from 'make-error';
import { fromJS } from 'immutable';

export default class ValidationError extends BaseError {

  constructor(name, params = {}) {
    super(`ValidationError: ${JSON.stringify({ name, params })}`);
    this.name = name;
    this.params = params;
    const field = {};
    field[params.prop] = name;
    this.field = fromJS(field);
  }

}
