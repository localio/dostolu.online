import isomorphicFetch from 'isomorphic-fetch';
// import { removeToken } from '../auth/token';

const RESPONSE_STATUS_SUCCESS = 'success';

// const redirectToLogin = (auth, response) => {
//   if (auth) {
//     removeToken();
//     if (window) {
//       window.location.reload();
//     }
//   } else {
//     throw new Error(response.errors || null);
//   }
// };

/**
 * Generic json api fetcher
 *
 * If auth is true - remove token and redirect to the auth page
 * @param {String} url
 * @param {Object} options
 * @param {bool} auth
 */
export default (logger) => (url, options) => isomorphicFetch(url, options)
  .catch(err => logger && logger.warn(err))
  .then(response => response.json())
  .then(response => {
    if (response.status !== RESPONSE_STATUS_SUCCESS) {
      logger && logger.warn(`${response.status} ${response.statusText}`);
    }
    return response;
  });
