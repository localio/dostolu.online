import Immutable from 'immutable';

export default links => Immutable.fromJS(links);
