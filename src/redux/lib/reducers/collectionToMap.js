import { Seq } from 'immutable';

const collectionToIndexed = items => {
  const obj = {};
  items.map(item => obj[item._id] = item);
  return obj;
};

export default (items, Model) => Seq(collectionToIndexed(items))
  .map(json => new Model(json))
  .toMap();
