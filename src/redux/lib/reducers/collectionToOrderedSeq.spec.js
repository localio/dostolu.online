import { expect } from 'chai';
import Immutable, { OrderedSet, Map } from 'immutable';
import collectionToOrderedSet from './collectionToOrderedSet';

const fixture = {
  data: [
    {
      _id: 1,
      data: 1
    }, {
      _id: 3,
      data: 2
    }, {
      _id: 2,
      data: 3
    }
  ],
  Model: class {
    /**
     * @param data
     * @returns {Map}
     */
    constructor(data) {
      return Immutable.fromJS(data);
    }
  }
};

describe('collectionToOrderedSet', () => {
  it('Should return OrderedSet instance', () => {
    const res = collectionToOrderedSet(fixture.data, fixture.Model);
    expect(res).to.be.an.instanceof(OrderedSet);
  });
  it('OrderedSet should have the proper size', () => {
    const res = collectionToOrderedSet(fixture.data, fixture.Model);
    expect(res.size).to.be.equal(fixture.data.length);
  });
  it('should turn every map item to the model instance', () => {
    const res = collectionToOrderedSet(fixture.data, fixture.Model).first();
    expect(res).to.be.an.instanceof(Map);
  });
  it('should return elements in the proper order', () => {
    const res = collectionToOrderedSet(fixture.data, fixture.Model).toJS();
    expect(res).to.be.deep.equal(fixture.data);
  });
});
