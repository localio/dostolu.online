import Immutable from 'immutable';

export default meta => Immutable.fromJS(meta);
