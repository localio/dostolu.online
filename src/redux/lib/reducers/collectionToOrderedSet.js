import { Seq } from 'immutable';

export default (items, Model) => Seq(items)
  .map(json => new Model(json))
  .toOrderedSet();
