import { expect } from 'chai';
import Immutable, { Map } from 'immutable';
import collectionToMap from './collectionToMap';

const fixture = {
  data: [
    {
      _id: 1,
      data: 1
    }, {
      _id: 3,
      data: 2
    }, {
      _id: 2,
      data: 3
    }
  ],
  Model: class {
    /**
     * @param data
     * @returns {Map}
     */
    constructor(data) {
      return Immutable.fromJS(data);
    }
  }
};

describe('collectionToMap', () => {
  it('Should return Map instance', () => {
    const res = collectionToMap(fixture.data, fixture.Model);
    expect(res).to.be.an.instanceof(Map);
  });
  it('Map should have the proper size', () => {
    const res = collectionToMap(fixture.data, fixture.Model);
    expect(res.size).to.be.equal(fixture.data.length);
  });
  it('should turn every map item to the model instance', () => {
    const res = collectionToMap(fixture.data, fixture.Model).first();
    expect(res).to.be.an.instanceof(Map);
  });
  it('should be possible to get element by id', () => {
    const res = collectionToMap(fixture.data, fixture.Model).get('3');
    expect(res.get('data')).to.be.equal(2);
  });
});
