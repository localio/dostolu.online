import app from './app/reducer';
import auth from './auth/reducer';
import config from './config/reducer';
import device from './device/reducer';
import ui from './ui/reducer';
import users from './users/reducer';
import address from './address/reducer';
import restaurants from './restaurants/reducer';
import intl from './intl/reducer';
import restaurant from './restaurant/reducer';
import menu from './menu/reducer';
import menuUi from './menuUi/reducer';
import basket from './basket/reducer';
import customer from './customer/reducer';
import order from './order/reducer';
import location from './location/reducer';

import { SIGN_OUT } from './auth/actions';
import combineReducers from 'redux/lib/combineReducers';
import fields from './lib/redux-fields/reducer';
// import { firebaseReducer as firebase } from './lib/redux-firebase';
import { routerReducer as routing } from 'react-router-redux/lib/reducer';
import { updateStateOnStorageLoad } from './configureStorage';

const resetStateOnSignOut = (reducer, initialState) => (state, action) => {
  // Reset app state on sign out, stackoverflow.com/q/35622588/233902.
  if (action.type === SIGN_OUT) {
    // Preserve state without sensitive data.
    state = {
      app: state.app,
      config: initialState.config,
      device: initialState.device,
      routing: state.routing // Routing state has to be reused.
    };
  }
  return reducer(state, action);
};

export default function configureReducer(initialState, platformReducers) {
  let reducer = combineReducers({
    ...platformReducers,
    app,
    auth,
    config,
    device,
    fields,
    // firebase,
    routing,
    ui,
    users,
    address,
    restaurants,
    intl,
    restaurant,
    menu,
    menuUi,
    basket,
    customer,
    order,
    location
  });

  // The power of higher-order reducers, http://slides.com/omnidan/hor
  reducer = resetStateOnSignOut(reducer, initialState);
  reducer = updateStateOnStorageLoad(reducer);

  return reducer;
}
