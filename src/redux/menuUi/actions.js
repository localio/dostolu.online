export const SET_ACTIVE_SECTION = 'SET_ACTIVE_SECTION';
export const CLEAN_ALL = 'CLEAN_ALL';
export const TOGGLE_MENU_PAGE = 'TOGGLE_MENU_PAGE';

export function cleanAll() {
  return {
    type: 'CLEAN_ALL'
  };
}

export function setActiveSection(id) {
  return {
    type: 'SET_ACTIVE_SECTION',
    payload: { id }
  };
}

export function cleanActiveSection() {
  return {
    type: 'SET_ACTIVE_SECTION',
    payload: { id: null }
  };
}

export function toggleMenuPage() {
  return {
    type: 'TOGGLE_MENU_PAGE'
  };
}
