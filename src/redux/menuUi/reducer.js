import * as actions from './actions';
import { Record } from '../transit';

const InitialState = Record({
  activeSection: null,
  nonActiveSection: null, // Will be applied for the mobile version only
  menuPage: false
}, 'menuUi');

export default function menuUiReducer(state = new InitialState, action) {
  switch (action.type) {
    case actions.CLEAN_ALL: {
      return state.set('activeSection', null);
    }
    case actions.SET_ACTIVE_SECTION: {
      const { id } = action.payload;
      // Same section has been clicked
      if (id === state.get('activeSection')) {
        // If section has been collapsed -> open it
        if (state.get('nonActiveSection') === id) {
          return state.set('activeSection', id).set('nonActiveSection', null);
        }
        // If section has been opened -> close its
        return state.set('activeSection', id).set('nonActiveSection', id);
      }
      // New section selected - clean up non active
      return state.set('activeSection', id).set('nonActiveSection', null);
    }
    case actions.TOGGLE_MENU_PAGE: {
      return state.set('menuPage', !state.get('menuPage'));
    }
  }
  return state;
}
