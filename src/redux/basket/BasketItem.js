import Immutable from 'immutable';

const BasketItem = class {
  constructor(data) {
    const { _id, quantity } = data;
    return Immutable.fromJS({
      _id, quantity
    });
  }
};

export default BasketItem;
