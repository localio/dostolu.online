import * as actions from './actions';
import { Record } from '../transit';
import Basket from './Basket';
import BasketItem from './BasketItem';
import { OrderedMap } from 'immutable';

const InitialState = Record({
  map: new OrderedMap()
}, 'basket');

export default function basketReducer(state = new InitialState, action) {
  switch (action.type) {
    case actions.CLEAN_BASKET: {
      return state.set('map', new OrderedMap());
    }
    case actions.ADD_ITEM: {
      const { itemId, price, restaurant } = action.payload;
      // First time - just set menu id and restaurant id
      if (!state.getIn(['map', restaurant])) {
        return state.setIn(['map', restaurant], new Basket(action.payload));
      }
      // get existing basket
      let basket = state.getIn(['map', restaurant]);
      // Increase count and total
      basket = basket
        .set('count', basket.get('count') + 1)
        .set('total', basket.get('total') + price);
      // If item is in, increase quantity
      if (basket.getIn(['items', itemId])) {
        basket = basket.setIn(['items', itemId, 'quantity'],
          basket.getIn(['items', itemId, 'quantity']) + 1
        );
      } else {
        // Or set new item to the basket
        basket = basket.setIn(['items', itemId], new BasketItem({
          _id: itemId, quantity: 1
        }));
      }
      return state.setIn(['map', restaurant], basket);
    }

    case actions.REMOVE_ITEM: {
      const { itemId, price, restaurant } = action.payload;
      let basket = state.getIn(['map', restaurant]);
      if (basket) {
        basket = basket
          .set('count', basket.get('count') - 1)
          .set('total', basket.get('total') - price);
        let item = basket.getIn(['items', itemId]);
        item = item.set('quantity', item.get('quantity') - 1);
        // If item quantity is 0 - remove item
        if (item.get('quantity') > 0) {
          basket = basket.setIn(['items', itemId], item);
        } else {
          basket = basket.removeIn(['items', itemId]);
        }
        if (basket.get('items').size > 0) {
          return state.setIn(['map', restaurant], basket);
        }
        // If there is nothing in the basket - remove basket
        return state.removeIn(['map', restaurant]);
      }
    }
  }
  return state;
}
