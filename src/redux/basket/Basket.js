import Immutable, { OrderedMap } from 'immutable';
import BasketItem from './BasketItem';

const Basket = class {
  constructor(data) {
    const { restaurant, menu, price, itemId } = data;
    return Immutable.fromJS({
      items: (new OrderedMap()).set(itemId, new BasketItem({
        _id: itemId,
        quantity: 1
      })),
      count: 1,
      total: price,
      restaurant,
      menu,
      started: Math.floor(Date.now())
    });
  }
};

export default Basket;
