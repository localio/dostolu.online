export const CLEAN_BASKET = 'CLEAN_BASKET';
export const ADD_ITEM = 'ADD_ITEM';
export const REMOVE_ITEM = 'REMOVE_ITEM';

export function cleanBasket() {
  return {
    type: 'CLEAN_BASKET'
  };
}

/**
 * Add item to card
 * @param {String} itemId
 * @param {Number} price
 * @param {String} menu
 * @param {String} restaurant
 * @returns {{type: string, payload: {itemId: *, menuId: *, restaurantId: *}}}
 */
export function addItem(itemId, price, menu, restaurant) {
  return {
    type: 'ADD_ITEM',
    payload: {
      itemId,
      price,
      menu,
      restaurant
    }
  };
}

export function removeItem(itemId, price, restaurant) {
  return {
    type: 'REMOVE_ITEM',
    payload: {
      itemId,
      price,
      restaurant
    }
  };
}
