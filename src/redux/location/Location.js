import Immutable from 'immutable';
import Address from '../address/Address';

const Location = class {
  constructor(data = null) {
    let address = null;
    if (data && 'addressModel' in data) {
      address = new Address(data.addressModel);
    }
    return Immutable.fromJS(data ? {
      _id: data._id,
      name: data.name,
      apartment: data.apartment,
      place: data.place,
      instructions: data.instructions,
      address
    } : {});
  }
};

export default Location;
