export const SET_LOCATION_NAME = 'SET_LOCATION_NAME';
export const SET_LOCATION_APARTMENT = 'SET_LOCATION_APARTMENT';
export const SET_LOCATION_PLACE = 'SET_LOCATION_PLACE';
export const SET_LOCATION_ID = 'SET_LOCATION_ID';
export const SET_LOCATION_INSTRUCTIONS = 'SET_LOCATION_INSTRUCTIONS';
export const CLEAN_LOCATION = 'CLEAN_LOCATION';

export function setLocationName(payload) {
  return {
    type: 'SET_LOCATION_NAME',
    payload
  };
}

export function setLocationApartment(payload) {
  return {
    type: 'SET_LOCATION_APARTMENT',
    payload
  };
}

export function setLocationPlace(payload) {
  return {
    type: 'SET_LOCATION_PLACE',
    payload
  };
}

export function setLocationInstructions(payload) {
  return {
    type: 'SET_LOCATION_INSTRUCTIONS',
    payload
  };
}

export function setLocationId(payload) {
  return {
    type: 'SET_LOCATION_ID',
    payload
  };
}

export function cleanLocation() {
  return {
    type: 'CLEAN_LOCATION'
  };
}
