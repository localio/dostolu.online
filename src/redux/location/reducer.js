import * as actions from './actions';
import Location from './Location';
import { Record } from '../transit';

const InitialState = Record({
  location: null
}, 'location');

const initState = state =>
  state.get('location') === null
    ? state.set('location', new Location())
    : state;

export default function locationReducer(state = new InitialState, action) {
  switch (action.type) {
    case actions.SET_LOCATION_NAME: {
      return initState(state)
        .setIn(['location', 'name'], action.payload);
    }
    case actions.SET_LOCATION_APARTMENT: {
      return initState(state)
        .setIn(['location', 'apartment'], action.payload);
    }
    case actions.SET_LOCATION_PLACE: {
      return initState(state)
        .setIn(['location', 'place'], action.payload);
    }
    case actions.SET_LOCATION_INSTRUCTIONS: {
      return initState(state)
        .setIn(['location', 'instructions'], action.payload);
    }
    case actions.SET_LOCATION_ID: {
      return initState(state)
        .setIn(['location', '_id'], action.payload);
    }
    case actions.CLEAN_LOCATION: {
      return new InitialState;
    }
  }
  return state;
}
