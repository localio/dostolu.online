// import { browserHistory } from 'react-router';
//
// // Check Firebase actions in src/common/lib/redux-firebase/actions.js
//
// export const SIGN_OUT = 'SIGN_OUT';
//
// export function signOut() {
//   return ({ firebaseAuth }) => {
//     // Always redirect to home first to ensure valid view state after sign out.
//     if (process.env.IS_BROWSER) {
//       browserHistory.replace('/');
//     }
//     firebaseAuth().signOut();
//     return {
//       type: SIGN_OUT
//     };
//   };
// }
// import { ValidationError } from '../../redux/lib/validation';
// import { removeToken, storeToken } from '../lib/auth/token';

export const LOGIN_ERROR = 'LOGIN_ERROR';
export const LOGIN_START = 'LOGIN_START';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGOUT = 'LOGOUT';

// export function login(fields) {
  // const data = AuthPresenter.render(fields);
  // return ({ fetch, getState }) => {
  //   const link = `${getState().config.api.path}/auth/login`;
  //   const promise = fetch(link, {
  //     method: 'POST',
  //     headers: {
  //       'Content-Type': 'application/vnd.api+json',
  //       'X-Forwarded-Proto': 'https'
  //     },
  //     body: JSON.stringify(data),
  //     mode: 'cors'
  //   }, false)
  //     .then(json => ({
  //       data: (new YaysonStore()).sync(json)
  //     }))
  //     .then(data => {
  //       storeToken(data.data.token);
  //       return data;
  //     });
  //   return {
  //     type: 'LOGIN',
  //     payload: promise
  //   };
  // };
// }
//
// export function logout() {
//   return () => {
//     removeToken();
//     return {
//       type: LOGOUT
//     };
//   };
// }
