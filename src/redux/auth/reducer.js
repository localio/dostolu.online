import { Record } from '../transit';
import * as loginActions from './actions';

const InitialState = Record({
  error: null,
  success: null, // To get accessToken, refreshToken, whatever.
}, 'auth');

export default function authReducer(state = new InitialState, action) {
  switch (action.type) {
    case loginActions.LOGIN_SUCCESS: {
      const { data } = action.payload;
      if (data.token) {
        return state.set('error', null).set('success', true);
      }
      return state;
    }
    case loginActions.LOGIN_ERROR: {
      return state.set('success', false);
    }
  }

  return state;
}
