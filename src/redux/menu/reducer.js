import * as actions from './actions';
import Menu from './Menu';
import { Record } from '../transit';
import Immutable, { Map } from 'immutable';

const InitialState = Record({
  menu: null,
  menuItems: null
}, 'menu');

export default function restaurantReducer(state = new InitialState, action) {
  switch (action.type) {
    case actions.CLEAN_MENU: {
      return state.set('menu', null).set('menuItems', null);
    }
    case actions.FETCH_MENU_SUCCESS: {
      const { data } = action.payload;
      let flatItems = new Map();
      if ('sections' in data) {
        data.sections.forEach(section => {
          if ('items' in section) {
            section.items.forEach(item => {
              flatItems = flatItems.set(item._id, Immutable.fromJS(item));
            });
          }
        });
      }
      return state
        .set('menu', new Menu(data))
        .set('menuItems', flatItems);
    }
  }
  return state;
}
