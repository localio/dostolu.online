import Immutable from 'immutable';

const MenuItem = class {
  constructor(data) {
    return Immutable.fromJS({
      _id: data._id,
      name: data.name,
      description: data.description,
      price: data.price
    });
  }
};

export default MenuItem;
