import Immutable from 'immutable';
import MenuSection from './MenuSection';
import collectionToOrderedSet from '../lib/reducers/collectionToOrderedSet';

const Menu = class {
  constructor(data) {
    return Immutable.fromJS({
      _id: data._id,
      sections: collectionToOrderedSet(data.sections, MenuSection)
    });
  }
};

export default Menu;
