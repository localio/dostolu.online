import Immutable from 'immutable';
import MenuItem from './MenuItem';
import collectionToOrderedSet from '../lib/reducers/collectionToOrderedSet';

const MenuSection = class {
  constructor(data) {
    return Immutable.fromJS({
      _id: data._id,
      name: data.name,
      items: collectionToOrderedSet(data.items, MenuItem)
    });
  }
};

export default MenuSection;
