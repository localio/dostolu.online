export const FETCH_MENU_ERROR = 'FETCH_MENU_ERROR';
export const FETCH_MENU_START = 'FETCH_MENU_START';
export const FETCH_MENU_SUCCESS = 'FETCH_MENU_SUCCESS';

export const CLEAN_MENU = 'CLEAN_MENU';

export function cleanMenu() {
  return {
    type: 'CLEAN_MENU'
  };
}

export function fetchMenuById() {
  return ({ fetch, getState }) => {
    let id = null;
    const restaurant = getState().restaurant.restaurant;
    if (restaurant) {
      id = restaurant.get('menu');
    }
    const promise = fetch(
      `${getState().config.api.path}/menus/${id}`, {
        method: 'GET',
        headers: {
          accept: 'application/json'
        },
        mode: 'cors'
      })
      .then(json => ({
        data: json.data
      }));
    return {
      type: 'FETCH_MENU',
      payload: promise
    };
  };
}
