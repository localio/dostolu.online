import Immutable from 'immutable';
import Cuisine from '../cuisine/Cuisine';

const RestaurantSearch = class {
  constructor(data) {
    return Immutable.fromJS({
      _id: data._id,
      name: data.name,
      slug: data.slug,
      orderMinimum: data.orderMinimum,
      openedToday: data.openedToday,
      status: data.status,
      logo: data.logo,
      cuisineModel: new Cuisine(data.cuisineModel)
    });
  }
};

export default RestaurantSearch;
