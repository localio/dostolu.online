import * as actions from './actions';
import RestaurantSearch from './RestaurantSearch';
import { Record } from '../transit';
import { isOnline, isOffline } from '../restaurant/lib/status';
import collectionToOrderedSet from '../lib/reducers/collectionToOrderedSet';

const initialSort = items => ({
  online: items.filter(obj => isOnline(obj.status)),
  offline: items.filter(obj => isOffline(obj.status))
});

const InitialState = Record({
  // searchResults: null // new Map()
  online: null,
  offline: null,
}, 'restaurants');

export default function restaurantsReducer(state = new InitialState, action) {
  switch (action.type) {
    case actions.SEARCH_SUCCESS: {
      const { data } = action.payload;
      const sortedData = initialSort(data);
      return state
        .set('online', collectionToOrderedSet(sortedData.online, RestaurantSearch))
        .set('offline', collectionToOrderedSet(sortedData.offline, RestaurantSearch));
    }
  }
  return state;
}
