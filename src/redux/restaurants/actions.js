import urlify from '../../common/lib/url/urlify';

export const SEARCH_ERROR = 'SEARCH_ERROR';
export const SEARCH_START = 'SEARCH_START';
export const SEARCH_SUCCESS = 'SEARCH_SUCCESS';

/**
 * Transform address information to the plain object
 * with the followinjg properties:
 * - placeId
 * - customerId
 * - townId
 * - townSlug
 * - streetId
 * - streetSlug
 * @param place
 */
const transformPlaceToBody = place => ({
  filters: {
    place: place.get('_id')
  }
});

export function search() {
  return ({ fetch, getState }) => {
    let params = '';
    if (getState().address.currentAddress) {
      params = urlify(transformPlaceToBody(
        getState().address.currentAddress.get('place')
      ));
    }

    const promise = fetch(
      `${getState().config.api.path}/search?${params}`, {
        method: 'GET',
        headers: {
          accept: 'application/json'
        },
        mode: 'cors'
      })
      .then(json => ({
        data: json.data
      }));
    return {
      type: 'SEARCH',
      payload: promise
    };
  };
}
