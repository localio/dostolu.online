import Immutable from 'immutable';

const Cuisine = class {
  constructor(data) {
    return Immutable.fromJS({
      _id: data._id,
      slug: data.slug,
      name: data.name
    });
  }
};

export default Cuisine;
