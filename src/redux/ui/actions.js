export const ON_SIDE_MENU_CHANGE = 'ON_SIDE_MENU_CHANGE';
export const TOGGLE_SIDE_MENU = 'TOGGLE_SIDE_MENU';
export const SET_CLIENT_RECT = 'SET_CLIENT_RECT';
export const SET_REACH_BOTTOM = 'SET_REACH_BOTTOM';
export const SET_LOADING = 'SET_LOADING';
export const TOGGLE_FIXED_BODY = 'TOGGLE_FIXED_BODY';
export const OFF_FIXED_BODY = 'OFF_FIXED_BODY';
export const SET_VIEWPORT_HEIGHT = 'SET_VIEWPORT_HEIGHT';

export function onSideMenuChange(isOpen) {
  return {
    type: ON_SIDE_MENU_CHANGE,
    payload: { isOpen }
  };
}

export function toggleSideMenu() {
  return {
    type: TOGGLE_SIDE_MENU
  };
}

export function setClientRect(rect) {
  return {
    type: SET_CLIENT_RECT,
    payload: { rect }
  };
}

export function setReachBottomFlag(flag) {
  return {
    type: SET_REACH_BOTTOM,
    payload: !!flag
  };
}

export function setLoading(element, value) {
  return {
    type: SET_LOADING,
    payload: { element, value }
  };
}

export function toggleFixedBody() {
  return {
    type: TOGGLE_FIXED_BODY
  };
}

export function offFixedBody() {
  return {
    type: OFF_FIXED_BODY
  };
}

export function setViewportHeight(height) {
  return {
    type: SET_VIEWPORT_HEIGHT,
    payload: { height }
  };
}
