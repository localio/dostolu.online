import * as actions from './actions';
import { Record } from '../transit';
import { Map } from 'immutable';

const InitialState = Record({
  isSideMenuOpen: false,
  fixedBody: false,
  clientRect: null,
  bottom: false,
  viewportHeight: null,
  loading: Map()
}, 'ui');

export default function uiReducer(state = new InitialState, action) {
  switch (action.type) {
    case actions.SET_VIEWPORT_HEIGHT: {
      const { height } = action.payload;
      return state.set('viewportHeight', height);
    }
    case actions.TOGGLE_FIXED_BODY: {
      return state.set('fixedBody', !state.get('fixedBody'));
    }
    case actions.OFF_FIXED_BODY: {
      return state.set('fixedBody', false);
    }
    case actions.ON_SIDE_MENU_CHANGE: {
      const { isOpen } = action.payload;
      return state.set('isSideMenuOpen', isOpen);
    }
    case actions.TOGGLE_SIDE_MENU:
      return state.update('isSideMenuOpen', isSideMenuOpen => !isSideMenuOpen);
    case actions.SET_CLIENT_RECT: {
      const { rect } = action.payload;
      if (rect) {
        return state.set('clientRect', (rect instanceof ClientRect) && rect);
      }
      break;
    }
    case actions.SET_REACH_BOTTOM:
      return state.set('bottom', action.payload);
    case actions.SET_LOADING: {
      const { element, value } = action.payload;
      if (value === false) {
        return state.deleteIn(['loading', element]);
      }
      return state.setIn(['loading', element], true);
    }
  }

  return state;
}
