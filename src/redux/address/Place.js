import Immutable from 'immutable';

const Place = class {
  constructor(data) {
    return Immutable.fromJS({
      _id: data._id,
      slug: data.slug,
      housenumber: data.housenumber
    });
  }
};

export default Place;
