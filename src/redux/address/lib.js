/**
 * Build address URL from Address model
 * @param {Address} Address
 * @returns {string}
 */
const getAddressUrl = (Address) =>
  `${Address.get('town').get('slug')}/${Address.get('street').get('slug')}`;

export {
  getAddressUrl
};
