export const FIND_ADDRESSES_ERROR = 'FIND_ADDRESSES_ERROR';
export const FIND_ADDRESSES_START = 'FIND_ADDRESSES_START';
export const FIND_ADDRESSES_SUCCESS = 'FIND_ADDRESSES_SUCCESS';

export const FETCH_ADDRESS_ERROR = 'FETCH_ADDRESS_ERROR';
export const FETCH_ADDRESS_START = 'FETCH_ADDRESS_START';
export const FETCH_ADDRESS_SUCCESS = 'FETCH_ADDRESS_SUCCESS';

export const CLEAR_ADDRESSES = 'CLEAR_ADDRESSES';
export const CHOOSE_ADDRESS = 'CHOOSE_ADDRESS';
export const SET_SHORT_ADDRESS = 'SET_SHORT_ADDRESS';

export function findAddresses(query) {
  return ({ fetch, getState }) => {
    const promise = fetch(
      `${getState().config.api.path}/addresses?q=${encodeURIComponent(query)}`, {
        method: 'GET',
        headers: {
          accept: 'application/json'
        },
        mode: 'cors'
      })
      .then(json => ({
        data: json.data.result
      }));
    return {
      type: 'FIND_ADDRESSES',
      payload: promise
    };
  };
}

export function clearAddresses() {
  return {
    type: CLEAR_ADDRESSES
  };
}

export function chooseAddress(id) {
  return {
    type: CHOOSE_ADDRESS,
    payload: {
      id
    }
  };
}

export function setShortAddress(shortAddress) {
  return {
    type: SET_SHORT_ADDRESS,
    payload: {
      shortAddress
    }
  };
}

export function fetchAddress() {
  return ({ fetch, getState }) => {
    // Get selected ID
    const id = getState().address.get('currentId');
    const entity = getState().address.get('currentAddress');
    // Don't fetch if it has been fetched already
    if (entity === null || entity.get('place').get('_id') !== id) {
      const promise = fetch(
        `${getState().config.api.path}/addresses/${id}`, {
          method: 'GET',
          headers: {
            accept: 'application/json'
          },
          mode: 'cors'
        })
        .then(json => ({
          data: json.data.result
        }));
      return {
        type: 'FETCH_ADDRESS',
        payload: promise
      };
    }
    return null;
  };
}
