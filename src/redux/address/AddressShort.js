import Immutable from 'immutable';

const AddressShort = class {
  constructor(data) {
    return Immutable.fromJS({
      _id: data._id,
      address: data.address
    });
  }
};

export default AddressShort;
