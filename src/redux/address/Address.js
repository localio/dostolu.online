import Immutable from 'immutable';
import Town from './Town';
import District from './District';
import Street from './Street';

const Address = class {
  constructor(data) {
    return Immutable.fromJS({
      town: new Town(data.town),
      district: new District(data.district),
      street: new Street(data.street),
      place: Immutable.fromJS(data),
    });
  }
};

export default Address;
