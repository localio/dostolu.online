import Immutable from 'immutable';

const Street = class {
  constructor(data) {
    return Immutable.fromJS({
      _id: data._id,
      slug: data.slug,
      name: data.name
    });
  }
};

export default Street;
