import * as actions from './actions';
import Address from './Address';
import AddressShort from './AddressShort';
import { Record } from '../transit';
import { Map } from 'immutable';
import collectionToOrderedSet from '../lib/reducers/collectionToOrderedSet';

const InitialState = Record({
  searchResults: Map(),
  currentAddress: null,
  currentShortAddress: null,
  currentId: null
}, 'address');
//
// export const reviveList = restaurants => restaurants && Seq(restaurants)
//   .map(json => new Restaurant(json))
//   .toMap();

export default function addressReducer(state = new InitialState, action) {
  switch (action.type) {
    case actions.FIND_ADDRESSES_SUCCESS: {
      const { data } = action.payload;
      const searchResults = collectionToOrderedSet(data, AddressShort);
      return state.set('searchResults', searchResults);
    }
    case actions.CLEAR_ADDRESSES: {
      return state.set('searchResults', Map()).set('currentShortAddress', null);
    }
    case actions.CHOOSE_ADDRESS: {
      const { id } = action.payload;
      return state.set('currentId', id);
    }
    case actions.FETCH_ADDRESS_SUCCESS: {
      const { data } = action.payload;
      return state.set('currentAddress', new Address(data));
    }
    case actions.SET_SHORT_ADDRESS: {
      const { shortAddress } = action.payload;
      return state.set('currentShortAddress', shortAddress);
    }
  }
  return state;
}
