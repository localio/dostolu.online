import gulp from 'gulp';
import runSequence from 'run-sequence';

gulp.task('messages', done => {
  runSequence(
    'messages-extract',
    'messages-clear',
    'messages-add',
    'messages-check',
    done);
});
