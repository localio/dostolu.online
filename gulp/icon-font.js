import gulp from 'gulp';
import iconfont from 'gulp-iconfont';
import consolidate from 'gulp-consolidate';

gulp.task('icon-font', (done) => {
  const runTimestamp = Math.round(Date.now() / 1000);

  gulp.src(['src/browser/assets/icons/*.svg'])
    .pipe(iconfont({
      fontName: 'ficons',
      prependUnicode: true,
      formats: ['ttf', 'eot', 'woff', 'svg'],
      normalize: true,
      fontHeight: 2000,
      timestamp: runTimestamp,
    }))
    .on('glyphs', (g) => {
      gulp.src('src/browser/assets/styles/base/elements/templates/_icons.scss')
        .pipe(consolidate('swig', {
          glyphs: g,
          fontName: 'ficons',
          fontPath: 'src/browser/assets/fonts/',
          cssClass: 'ico'
        }))
        .pipe(gulp.dest('src/browser/assets/styles/base/elements/', { overwrite: true }));
        // .on('finish', done);
    })
    .pipe(gulp.dest('src/browser/assets/fonts/'))
    .on('end', done);
});
