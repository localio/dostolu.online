import gulp from 'gulp';
import purge from 'gulp-css-purge';
import minify from 'gulp-minify-css';

gulp.task('build', ['build-webpack'], () => {
  gulp.src(['build/**/*.css'])
    .pipe(purge())
    .pipe(minify())
    .pipe(gulp.dest('build'));
});
