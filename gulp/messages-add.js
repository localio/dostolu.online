/* eslint-disable no-console */
import fs from 'fs';
import gulp from 'gulp';
import { diff, messagesToCode } from './support/messages';

gulp.task('messages-add', [/* 'messages-extract', 'messages-clear' */], () => {
  const loadMessages = require('../src/server/intl/loadMessages');
  const messages = loadMessages({ includeDefault: true });
  const defaultMessagesKeys = Object.keys(messages._default);
  const defaultMessages = messages._default;

  Object.keys(messages)
    .filter(locale => locale !== '_default')
    .forEach(locale => {
      const localeMessagesKeys = Object.keys(messages[locale]);
      const missingMessagesKeys = diff(defaultMessagesKeys, localeMessagesKeys);
      if (!missingMessagesKeys.length) return;
      const missingMessages = [];
      missingMessagesKeys.forEach(key => {
        missingMessages.push({
          id: key,
          defaultMessage: defaultMessages[key]
        });
      });
      console.log(locale);
      console.log(missingMessagesKeys);
      const file =
        (require(`../messages/${locale}`).default)
        .concat(missingMessages);
      const code = messagesToCode(file);
      fs.writeFile(`messages/${locale}.js`, code);
    });
});
